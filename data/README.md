Die hier bereitgestellten Daten sind eine leicht veränderte Version des Datensatzes Verwaltungsgebiete 1:5 000 000, Stand 01.01. (VG5000 01.01.) © GeoBasis-DE / BKG (2024).

Jedem Objekt wurden 5 Felder "t1" bis "t5" hinzugefügt. Die darin enthaltenen Werte bilden einen Auszug aus dem Infektionsgeschehen der Corona-Pandemie in Deutschland (Neuinfektionen pro Hundertausend Einwohner). Zusätzlich wurden willkürlich ein paar kleine Inseln entfernt. Mithilfe dieses Datensatzes können beispielhafte Visualisierungen von Änderungen erzeugt werden.

Die originalen Daten sind unter der [Datenlizenz Deutschland – Namensnennung – Version 2.0](https://www.govdata.de/dl-de/by-2-0) unter [https://gdz.bkg.bund.de/index.php/default/verwaltungsgebiete-1-5-000-000-stand-01-01-vg5000-01-01.html](https://gdz.bkg.bund.de/index.php/default/verwaltungsgebiete-1-5-000-000-stand-01-01-vg5000-01-01.html) verfügbar. 
