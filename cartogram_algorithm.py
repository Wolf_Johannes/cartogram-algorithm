import fiona
from shapely.geometry import LineString, Polygon, MultiPolygon, Point, mapping, base, shape
from shapely.ops import unary_union, split
from shapely.validation import make_valid
from shapely.strtree import STRtree
from shapely.affinity import scale, translate
import math as m
import multiprocessing
import sys
from pyqtree import Index
from scipy.interpolate import LinearNDInterpolator
import numpy as np
from functools import partial
import time
import signal
from frechetdist import frdist
import copy
import matplotlib.pyplot as plt
from matplotlib.path import Path
from matplotlib.patches import PathPatch
from matplotlib.collections import PatchCollection
from matplotlib import cm
import matplotlib.cbook as cbook
import matplotlib.colors as colors

sys.setrecursionlimit(100000000)

PATH_SHP = "data.\Einpersonenhaushalte.shp"#"Deutschland_Inseln_reduziert.shp" #"data.\Deutschland_Inseln_reduziert.shp" 
FIELD_NAME = "jahr_2018"#"t1_starkAb" #"t1_zunahme" #"t1_abnahme" #"t1_starkAb" 
FIELD_NAME2 = "jahr_2019"#"t2_starkAb" #t2_zunahme" #"t2_abnahme" #"t2_starkAb"
CALC_METHOD = "standardized_relative_change" #"standardized_relative_change"
FAKTOR = 1
EXPONENT = 20
RMSRE_LIMIT = 0.01
MAX_ITERATIONS = 1
PATH_OUTPUT_SHP = "OUTPUT_Einpersonenhaushalte_1ITER_test.shp" #A1_10iter_1exp_1fakt_5maxdepth_3enl_3shrink_Fishnet.shp" #"output_final\D_5iter_1exp_1fakt_6maxdepth_3enl_3shrink.shp" #"E_5iter_1exp_1fakt_6maxdepth.shp" #"output\Final_Deutschland_starke_Zunahme_relative_change_rmsrelimit0_01_faktor1_exponent075_maxiter5_enlarge5_shrink3_circlepoints8_maxdepth6_maxitems1_sizecorrection1_variabledepth0.shp"
ENLARGE = 3
SHRINK = 3
CIRCLE_POINTS = 8
MAX_DEPTH = 2
MAX_ITEMS = 1
TOTAL_SIZE_CORRECTION = True
VARIABLE_DEPTH = False
MIN_PORTION = 0.2


def init_worker():
    signal.signal(signal.SIGINT, signal.SIG_IGN)

def func(x_q, y_q, centroid_x, centroid_y, x_pi, y_pi, r_j_e, d_pi_ui):
    x_ui = centroid_x + r_j_e * (x_pi - centroid_x)
    y_ui = centroid_y + r_j_e * (y_pi - centroid_y)
    d_pi_q = m.sqrt((x_q - x_pi) ** 2 + (y_q - y_pi) ** 2)
    soll_betrag = d_pi_ui * m.e ** (-d_pi_q / d_pi_ui)
    ist_betrag = m.sqrt((x_ui - x_pi) ** 2 + (y_ui - y_pi) ** 2)
    return np.array([(x_ui - x_pi) * (soll_betrag / ist_betrag), (y_ui - y_pi) * (soll_betrag / ist_betrag)])

def zero_func(x_q, y_q):
    return np.array([0, 0])

def force_function(x_pi, y_pi, centroid_x, centroid_y, area_ist, area_soll, enlarge, shrink):
    """Bestimmt die Variablen der Kräfte-Funktion, die von den Parametern enlarge und shrink abhängen (Force-Enhancement).
    
    Parameters:
        x_pi (Float): X-Koordinate des Ausgangspunktes (auf Kreis um Zentroid)
        y_pi (Float): Y-Koordinate des Ausgangspunktes (auf Kreis um Zentroid)
        centroid_x (Float): X-Koordinate des Kreismittelpunktes
        centroid_y (Float): Y-Koordinate des Kreismittelpunktes
        area_ist (Float): Fläche des Ausgangskreises (IST)
        area_soll (Float): Fläche des Skalierten Kreises (SOLL)
        enlarge (Float): Faktor, der das Vergrößern mitbestimmt
        shrink (Float): Faktor, der das Verkleinern mitbestimmt
    
    Returns:
        partial: Funktion als Partial-Objekt
    """
    r_j = m.sqrt(area_soll / area_ist)
    p_enlarge = enlarge
    p_shrink = shrink
    if r_j >= 1:
        r_j_e = 1 + p_enlarge * (m.e ** (r_j - 1) - 1)
    elif r_j < 1:
        r_j_e = r_j * (1 + p_shrink * m.log(r_j))
    x_ui = centroid_x + r_j_e * (x_pi - centroid_x)
    y_ui = centroid_y + r_j_e * (y_pi - centroid_y)
    d_pi_ui = m.sqrt((x_pi - x_ui) ** 2 + (y_pi - y_ui) ** 2)
    force = partial(func, centroid_x=centroid_x, centroid_y=centroid_y, x_pi=x_pi, y_pi=y_pi,
                     r_j_e=r_j_e, d_pi_ui=d_pi_ui)

    zero_force = partial(zero_func)

    if d_pi_ui == 0:
        force = zero_force

    return force

def add_summand(func1, func2):
    return partial(sum_funcs, func1=func1, func2=func2)

def sum_funcs(x_q, y_q, func1, func2):
    """Summiert die Rückgabewerte zweier Funktionen bei gleichen Eingabeparametern x_q, y_q (X-, und Y-Koordinaten)"""
    return func1(x_q, y_q) + func2(x_q, y_q)

def diff_x(func, x, y):
    """Ableitung der Funktion func nach x an der Stelle x, y"""
    h = 1e-6
    return (func(x+h, y)-func(x-h, y))/(2*h)

def diff_y(func, x, y):
    """Ableitung der Funktion func nach y an der Stelle x, y"""
    h = 1e-6
    return (func(x, y+h)-func(x, y-h))/(2*h)

def calc_new_coordinates(quad, force_field_func, c_global):
    """Berechnet neue Koordinaten von (Quad-)Polygonen unter Anwendung eines, mit einem Faktor versehenden, Vektorfeldes.
    
    Parameters:
        quad (Shapely-Polygon): Quad-Zelle
        force_field_function (Function): Funktion mit Parametern X-, und Y-Koordinate und Rückgabewert als Vektor (Tupel) in X-, und Y-Richtung
        c_global (Float): Faktor für Verschiebung in Richtung des Vektors aus force_field_funktion

    Returns:
        Shapely-Polygon: Transformierte Quad-Zelle
    """
    quad_points = mapping(quad)['coordinates'][0]
    point_list = []
    for point in quad_points:
        point_list.append((point[0] + c_global * force_field_func(point[0], point[1])[0],
                            point[1] + c_global * force_field_func(point[0], point[1])[1]))
        
    new_quad = Polygon(point_list)
    return new_quad

def calc_local_elasticity_coefficients(point, force_field_func):
    """Berechnet lokalen Elastizitätskoeffizienten eines Punktes in einem Vektorfeld
    
    Parameters:
        point (Tupel): Tupel aus X-, und Y-Koordinaten eines Punktes
        force_field_function (Function): Funktion mit Parametern X-, und Y-Koordinate und Rückgabewert als Vektor (Tupel) in X-, und Y-Richtung

    Returns:
        Float: Lokaler Elastizitätskoeffizient
    """
    epsilon = sys.float_info.epsilon

    x_diff = diff_x(force_field_func, point[0], point[1])
    y_diff = diff_y(force_field_func, point[0], point[1])
    eval_force_x_dx = x_diff[0]
    eval_force_y_dx = x_diff[1]
    eval_force_x_dy = y_diff[0]
    eval_force_y_dy = y_diff[1]
    a = eval_force_x_dx * eval_force_y_dy - eval_force_x_dy * eval_force_y_dx
    b = eval_force_x_dx + eval_force_y_dy
    c_local = None

    if a==0 and b<0:
        c_local = (epsilon - 1)/b
    elif a!=0 and b**2>4*a:
        c_local = (-b-m.sqrt((b**2)-(4*a*(1-epsilon))))/(2*a)
    else:
        c_local = 1
    if c_local < 0 or c_local > 1:
        c_local = 1

    return c_local

def input_polygon_shape_file(path):
    """Liest eine ESRI-Shape-Datei bestehend aus Polygonen ein. Polygone werden entsprechend ihrer "shells" und "holes" in Shapely-Geometrien überführt.
    MultiPolygone werden für die weitere Verarbeitung in einzelne Polygone zerlegt. Die Information zusammengehöriger Polygone bleibt jedoch durch den Rückgabewert related_partial_polygons erhalten.
    Zusätzliche Informationen die für späteres Schreiben in eine ESRI-Shape-Datei notwendig sein können werden ebenfalls zurückgegeben.
    
    Parameters:
        path (String): Pfad zur ESRI-Shape-Datei, die Polygone enthält
    
    Returns:
        shapely_input (List): Eine Liste von Dictionaries, die die eingelesenen Polygone und ihre Eigenschaften enthalten.
        related_partial_polygons (List): Eine Liste von Tupeln, die jeweils die ID's aller zusammengehöriger Polygone enthält. Alle Polygone eines Tupels entstammen dem selben Eingangs-MultiPolygon.
        schema (Dictionary): Ein Dictionary, das das Schema der Shape-Datei enthält.
        crs (Dictionary): Das Koordinatenreferenzsystem (CRS) der Shape-Datei.
    """
    raw_input = []
    related_partial_polygons = []
    shapely_input = []
    with fiona.open(path, "r") as src:
        schema = src.schema
        crs = src.crs
        for i in src:
            if i['geometry']['type'] == 'Polygon':
                if len(i['geometry']['coordinates']) == 1:
                    raw_input.append(dict(i))
                    shapely_input.append(dict(i))
                    shapely_input[-1]['geometry'] = Polygon(dict(i)['geometry']['coordinates'][0])
                    shapely_input[-1]['properties'] = dict(i['properties'])
                else:
                    raw_input.append(dict(i))
                    shapely_input.append(dict(i))
                    shapely_input[-1]['geometry'] = Polygon(shell = dict(i)['geometry']['coordinates'][0], holes = dict(i)['geometry']['coordinates'][1:])
                    shapely_input[-1]['properties'] = dict(i['properties'])
            elif i['geometry']['type'] == 'MultiPolygon':
                multipolygon = MultiPolygon(shape(dict(i)['geometry']))
                sub_id = 0
                related_partial_polygons.append([])
                for poly in multipolygon.geoms:
                    raw_input.append(dict(i))
                    shapely_input.append(dict(i))
                    shapely_input[-1]['geometry'] = poly
                    shapely_input[-1]['properties'] = dict(i['properties'])
                    new_id = str(dict(i)['id']) + "_" + str(sub_id)
                    shapely_input[-1]['id'] = new_id
                    related_partial_polygons[-1].append((new_id, poly.area))
                    sub_id += 1
    print("Einlesen abgeschlossen! ...")
    return shapely_input, related_partial_polygons, schema, crs

def write_shape_file(data, schema, crs, path='output_test_master.shp'):
    """Schreibt die übergebene Daten in eine Shape-Datei.

    Parameters:
        data (List):        Eine Liste von Objekten, die in die Shape-Datei geschrieben werden sollen.
        schema(Dictionary): Das Schema für die Shape-Datei. Es definiert die Attribute der Features.
        crs (Dictionary):   Das Koordinatenreferenzsystem (Fiona) der Shape-Datei.
        path (String):      Der Pfad zur ESRI-Shape-Datei.

    Returns:
        Boolean:            Bei Erfolg wird True zurückgegeben. Andernfalls False.
    """
    try:
        with fiona.open(path, 'w', 'ESRI Shapefile', schema, crs) as c:
            for object in data:
                if isinstance(object['geometry'], base.BaseGeometry) and object['geometry'].is_valid:
                    c.write({
                            'geometry': mapping(object['geometry']),
                            'properties': object['properties'],
                        })
                elif isinstance(object['geometry'], base.BaseGeometry):
                    print("Die Geometrie ist ungültig und kann nicht gespeichert werden.")
                    print(object['geometry'])
    except Exception as e:
        print("FEHLER: Geometrie-Liste konnte nicht gespeichert werden! ...")
        print("Exception: ", e)
        return False
    print("Ergebnis wurde in Shape-Datei gespeichert! ...")
    return True

def write_list_to_shape_file(data, data_type, crs, path):
    """
    Schreibt eine Liste von Geometrien in eine Shape-Datei.

    Parameters:
        data (List):        Eine Liste von Geometrien, die in die Shape-Datei geschrieben werden sollen.
        data_type (String): Der Geometrietyp in der data-Liste. Gültige Werte sind 'Polygon', 'LineString' oder 'Point'.
        crs (Dictionary):   Das Koordinatenreferenzsystem (CRS) der Geometrien.
        path (String):      Der Dateipfad, an dem die Shape-Datei gespeichert werden soll.

    Returns:
        Boolean:            Bei Erfolg wird True zurückgegeben. Andernfalls False.
    """
    schema = None
    
    if data_type == 'Polygon':
        schema = {'geometry': 'Polygon','properties': {'ID': 'int'}}
    elif data_type == 'LineString':
        schema = {'geometry': 'LineString','properties': {'ID': 'int'}}
    elif data_type == 'Point':
        schema = {'geometry': 'Point','properties': {'ID': 'int'}}
    try:
        if schema != None:
            id = 0
            with fiona.open(path, 'w', 'ESRI Shapefile', schema, crs) as c:
                for object in data:
                    c.write({
                            'geometry': mapping(object),
                            'properties': {'ID': id},
                        })
                    id = id + 1
    except:
        print("FEHLER: Geometrie-Liste konnte nicht gespeichert werden! ...")
        return False
    print("Geometrie-Liste wurde in Shape-Datei gesichert! ...")
    return True

def calculate_scaled_areas(data, fieldname, schema, fieldname2=None, fieldname_scaled_areas="Scaled", fieldname_portion_of_country = "Portion", method="standardized_absolute_values", faktor=1, exponent=1, min_flaeche=1, related_partial_polygons=[]):
    """
    Berechnet skalierte Flächengrößen basierend auf den übergebenen Daten und der gewählten Berechnungsmethode.

    Parameters:
        data (List): Eine Liste von Objekten, die geometrische Informationen (als Shapely-Polygone) und weitere Eigenschaften enthalten.
        fieldname (String): Der Name des Feldes, das für die Skalierung verwendet wird.
        schema (Dictionary): Ein Schema, das die Struktur der Daten definiert.
        fieldname_scaled_areas (String, optional): Der Name des Feldes, in dem die skalierten Flächengrößen gespeichert werden. Standardmäßig ist der Name "Scaled".
        method (String): Methode der Ziel-Flächengrößenberechnung; 
            - "standardized_absolute_values" (DEFAULT) = Die Flächengröße ist proportional zu absoluten (positiven) Werten. Die Gesamtfläche bleibt erhalten. Es wird nur der Wert des Feldes "fieldname1" benutzt.
            - "standardized_relative_change" = Die Flächengrößenänderung entspricht der normierten (auf Gesamtwertänderung) prozentualen Wertänderung. Die Gesamtfläche bleibt erhalten.
            - "relative_change" = Die Flächengrößenänderung entspricht der prozentualen Wertänderung.
        faktor (Float): Faktor wird dem darzustellenden Wert angebracht (je nach Methode Wert oder Werteänderung). Hat nur Einfluss bei nicht-normierten Berechnungsmethoden.
        exponent (Float): Exponent wird darzustellenden Wert angebracht (je nach Methode Wert oder Werteänderung).
        min_flaeche (Float): Minimaler Flächeninhalt eines entstehenden Polygons. Damit Polygone nicht verschwinden können, sollte hier ein positiver Wert ungleich 0 gewählt werden.
        related_partial_polygons (List): Liste enthält Tupel mit ID's zusammengehöriger Polygone.
    Returns:
        data (List): Die Liste der aktualisierten Objekte mit den skalierten Flächengrößen.
        schema (Dictionary): Das aktualisierte Schema mit dem neuen Feld für die skalierten Flächengrößen.
    """
    if method == "standardized_absolute_values":
        sum_area_values = 0
        sum_field_values = 0
        for object in data:
            area_value = float(object['geometry'].area)
            field_value = float(object["properties"][fieldname])
            for relations in related_partial_polygons:
                if object['id'] in [related_id[0] for related_id in relations]:
                    sum_related_areas = sum([related_id[1] for related_id in relations])
                    portion_of_related_area_sum = float(object['geometry'].area)/sum_related_areas
                    field_value = float(object["properties"][fieldname]) * portion_of_related_area_sum
                    break
                else: 
                    field_value = float(object["properties"][fieldname])
            sum_field_values += faktor * field_value**exponent
            sum_area_values += area_value
        
        for object in data:
            area_value = float(object['geometry'].area)
            portion_of_related_area_sum = 1
            if fieldname_scaled_areas not in object["properties"].keys():
                for relations in related_partial_polygons:
                    if object['id'] in [related_id[0] for related_id in relations]:
                        sum_related_areas = sum([related_id[1] for related_id in relations])
                        portion_of_related_area_sum = float(object['geometry'].area)/sum_related_areas
                        break
                    else:
                        portion_of_related_area_sum = 1

            new_area = sum_area_values * (faktor * float(object["properties"][fieldname])**exponent) / sum_field_values * portion_of_related_area_sum

            if fieldname_scaled_areas not in object["properties"].keys() and new_area <= 0:
                object["properties"][fieldname_scaled_areas] = 1
            elif fieldname_scaled_areas not in object["properties"].keys():
                object["properties"][fieldname_scaled_areas] = new_area
        schema['properties'][fieldname_scaled_areas] = 'float'
        return data, schema
    
    elif method == "relative_change" and isinstance(fieldname2, str): 
        sum_area_values = 0
        for object in data:
            area_value = float(object['geometry'].area)
            value_change = float(object["properties"][fieldname2])/float(object["properties"][fieldname])
            if fieldname_scaled_areas in object["properties"].keys() and float(object["properties"][fieldname_scaled_areas])>0:
                area_value = float(object["properties"][fieldname_scaled_areas])
            else:
                for relations in related_partial_polygons:
                    if object['id'] in [related_id[0] for related_id in relations]:
                        sum_related_areas = sum([related_id[1] for related_id in relations])
                        portion_of_related_area_sum = float(object['geometry'].area)/sum_related_areas
                        area_value = float(object['geometry'].area) * portion_of_related_area_sum
                        break
                    else: 
                        area_value = float(object['geometry'].area)
            sum_area_values += area_value
        for object in data:
            area_value = float(object['geometry'].area)
            value_change = float(object["properties"][fieldname2])/float(object["properties"][fieldname])
            if fieldname_scaled_areas in object["properties"].keys() and float(object["properties"][fieldname_scaled_areas])>0:
                area_value = float(object["properties"][fieldname_scaled_areas])
            else:
                for relations in related_partial_polygons:
                    if object['id'] in [related_id[0] for related_id in relations]:
                        sum_related_areas = sum([related_id[1] for related_id in relations])
                        portion_of_related_area_sum = float(object['geometry'].area)/sum_related_areas
                        area_value = float(object['geometry'].area) * portion_of_related_area_sum
                        break
                    else:
                        area_value = float(object['geometry'].area)

            new_area = area_value * faktor * (value_change)**exponent

            if fieldname_scaled_areas not in object["properties"].keys() and new_area <= 0:
                object["properties"][fieldname_scaled_areas] = 1
            elif fieldname_scaled_areas not in object["properties"].keys():
                object["properties"][fieldname_scaled_areas] = new_area
        schema['properties'][fieldname_scaled_areas] = 'float'
        return data, schema
    
    elif method == "standardized_relative_change" and isinstance(fieldname2, str): 
        sum_area_values = 0
        sum_area_values1 = 0
        for object in data:
            area_value = float(object['geometry'].area)
            value_change = float(object["properties"][fieldname2])/float(object["properties"][fieldname])
            if fieldname_scaled_areas in object["properties"].keys() and float(object["properties"][fieldname_scaled_areas])>0:
                area_value = float(object["properties"][fieldname_scaled_areas])
            else:
                for relations in related_partial_polygons:
                    if object['id'] in [related_id[0] for related_id in relations]:
                        area_value = float(object['geometry'].area)
                        #sum_related_areas = sum([related_id[1] for related_id in relations])
                        """print("DEBUG")
                        print("Area: ", float(object['geometry'].area))
                        print([related_id[1] for related_id in relations])
                        print("Sum_Related_Areas: ", sum_related_areas)"""
                        #portion_of_related_area_sum = float(object['geometry'].area)/sum_related_areas
                        #print("portion_of_related_area_sum: ", portion_of_related_area_sum)
                        #area_value = float(object['geometry'].area) * portion_of_related_area_sum
                        #print("area_value: ", area_value)
                        break
                    else:
                        area_value = float(object['geometry'].area)
            sum_area_values += area_value
            sum_area_values1 += faktor * area_value * (value_change)**exponent
        for object in data:
            area_value = float(object['geometry'].area)
            value_change = float(object["properties"][fieldname2])/float(object["properties"][fieldname])
            portion_of_related_area_sum = 1
            if fieldname_scaled_areas in object["properties"].keys() and float(object["properties"][fieldname_scaled_areas])>0:
                area_value = float(object["properties"][fieldname_scaled_areas])
            else:
                for relations in related_partial_polygons:
                    if object['id'] in [related_id[0] for related_id in relations]:
                        """sum_related_areas = sum([related_id[1] for related_id in relations])
                        portion_of_related_area_sum = float(object['geometry'].area)/sum_related_areas"""
                        sum_related_areas = sum([related_id[1] for related_id in relations])
                        portion_of_related_area_sum = float(object['geometry'].area)/sum_related_areas
                        area_value = float(object['geometry'].area) #* portion_of_related_area_sum
                        break
                    else:
                        portion_of_related_area_sum = 1
                        area_value = float(object['geometry'].area)

            new_area = faktor * area_value * (value_change)**exponent * sum_area_values / sum_area_values1
            """print("name: ", object["properties"]["gen"])
            print("area_value; ", area_value)
            print("faktor: ", faktor)
            print("exponent: ", exponent)
            print("value_change: ", value_change)
            print("sum_area_values: ", sum_area_values)
            print("sum_area_values1: ", sum_area_values1)
            print("new_area: ", new_area)"""

            if fieldname_scaled_areas not in object["properties"].keys() and new_area <= 0:
                object["properties"][fieldname_scaled_areas] = 1
            elif fieldname_scaled_areas not in object["properties"].keys():
                object["properties"][fieldname_scaled_areas] = new_area
            
            if fieldname_portion_of_country not in object["properties"].keys():
                object["properties"][fieldname_portion_of_country] = portion_of_related_area_sum

        schema['properties'][fieldname_scaled_areas] = 'float'
        schema['properties'][fieldname_portion_of_country] = 'float'
        return data, schema
    else:
        raise ValueError("Ungültige Berechnungsmethode! Bitte überprüfe die Schreibweise der übergebenen CALC_METHOD")

def extract_points(data):
    """
    Extrahiert Punkte aus den gegebenen Daten und erstellt eine Zuordnung von Punkten zu den zugehörigen Polygon-IDs und Positionen.

    Parameters:
        data (List): Eine Liste von Objekten, die geometrische Informationen (als Shapely-Polygone) und weitere Eigenschaften enthalten.

    Returns:
        points (Dictionary): Ein Dictionary, das die extrahierten Punkte enthält. Jeder Punkt (x,y) wird als Schlüssel verwendet und jedem Punkt ist eine Liste von Tupeln zugeordnet. Jedes Tupel enthält die Polygon-ID und eine Liste von Positionen, an denen der Punkt in dem Polygon gefunden wurde.

    Hinweise:
    - Die Funktion verwendet die äußeren Koordinaten (exterior) jedes Objekts in den Daten, um Punkte zu extrahieren.
    - Die Positionen der Punkte in den Polygons werden von 0 an aufsteigend nummeriert.

    Beispiele:
    - extract_points(data) gibt zurück:
        {
            (x1, y1): [(country_ID1, [position1, position2, ...]), (country_ID2, [position1, position2, ...]), ...],
            (x2, y2): [(country_ID3, [position1, position2, ...]), ...],
            ...
        }

    - Der Parameter data könnte wie folgt aussehen:
        data = [
        {
            'id': 'polygon1',
            'geometry': {
                'type': 'Polygon',
                'coordinates': [
                    [(0, 0), (0, 5), (5, 5), (5, 0), ..., (0, 0)]
                ]
            }
        },
        {
            'id': 'polygon2',
            'geometry': {
                'type': 'Polygon',
                'coordinates': [
                    [(2, 2), (2, 4), (4, 4), (4, 2), ..., (2, 2)]
                ]
            }
        },
        ...
        ]
    """
    points = {}
    for object in data:
        point_position = 0
        point_list_without_last = list(object['geometry'].exterior.coords)
        point_list_without_last.pop()
        for point in point_list_without_last:
            if point not in points:
                points[point] = [(object['id'], [point_position])]
                point_position += 1
            else: # Wenn der Punkt bereits extrahiert wurde muss nun geprüft werden, ob die entsprechende Polygon-ID ebenfalls registriert wurde
                for index, element in enumerate(points[point]):
                    id_already_in_points = False
                    if object['id'] == element[0]:
                        points[point][index][1].append(point_position)
                        id_already_in_points = True
                        break
                if not id_already_in_points:
                    points[point].append((object['id'], [point_position]))
                point_position += 1
    print("Punkte wurden extrahiert! ...")
    return(points)

#METHODE WIRD GERADE ERWEITERT!!!
#Funktionsfähigkeit ist nicht beeinträchtigt.
def extract_centroid_with_scaled_area(data, fieldname_scaled_area="Scaled", fieldname_portion_of_country="Portion"):
    """
    Extrahiert die Zentroide der Objekte aus den gegebenen Daten und gibt sie zusammen mit dem skalierten Bereich zurück.
    An dieser Stelle wäre denkbar, dass mithilfe eines Fishnet-Overlays zusätzliche krafterzeugende Polygone erzeugt werden. 
    Teile dieser Methode sind experimentell!

    Parameters:
        data (List): Eine Liste von Objekten, die geometrische Informationen (als Shapely-Polygone) und weitere Eigenschaften enthalten.
        fieldname_scaled_area (String, optional): Der Name des Feldes, das die skalierte Fläche in den Eigenschaften jedes Objekts enthält. Standardmäßig lautet der Name "Scaled".

    Returns:
        List: Eine Liste von Tupeln, wobei jedes Tupel die x- und y-Koordinaten des Zentoids, die skalierte Fläche und die Geometrie des Objekts als Shapely-Polygon enthält.

    Beispiel:
    - data = [
        {
            'geometry': <polygon1>,
            'properties': {
                'Scaled': 0.5,
                'other_property': 'value1'
            }
        },
        {
            'geometry': <polygon2>,
            'properties': {
                'Scaled': 0.8,
                'other_property': 'value2'
            }
        }
    ]

    - extract_centroid_with_scaled_area(data) gibt zurück:
        [
            (x1, y1, 0.5, <polygon1>),
            (x2, y2, 0.8, <polygon2>)
        ]

    """
    number_x_meshes = 7
    number_y_meshes = 7
    objects = []
    centroids = [] #delete wenn fertig
    centroids_new = []
    for object in data:
        objects.append(object['geometry'])
    bb = unary_union(objects).envelope
    coords = mapping(bb)['coordinates']
    min_x = coords[0][0][0]
    max_x = coords[0][2][0]
    min_y = coords[0][0][1]
    max_y = coords[0][2][1]
    diffX = max_x - min_x
    diffY = max_y - min_y
    x_per_mesh = diffX / number_x_meshes
    y_per_mesh = diffY / number_y_meshes


    #Fishnet-Overlay
    for object in data:
        if object['properties'][fieldname_portion_of_country] > 0.5:
            polygon = object['geometry']
            polygon_area = polygon.area
            obb = polygon.minimum_rotated_rectangle
            obb_coords = list(obb.exterior.coords)[:-1]
            midpoints = [
                ((obb_coords[0][0] + obb_coords[1][0]) / 2, (obb_coords[0][1] + obb_coords[1][1]) / 2),
                ((obb_coords[1][0] + obb_coords[2][0]) / 2, (obb_coords[1][1] + obb_coords[2][1]) / 2),
                ((obb_coords[2][0] + obb_coords[3][0]) / 2, (obb_coords[2][1] + obb_coords[3][1]) / 2),
                ((obb_coords[3][0] + obb_coords[0][0]) / 2, (obb_coords[3][1] + obb_coords[0][1]) / 2)
            ]
            obb_midline1 = LineString([midpoints[0], midpoints[2]])
            obb_midline2 = LineString([midpoints[1], midpoints[3]])

            if obb_midline1.length < obb_midline2.length:
                split_polygons = split(polygon, obb_midline1)
            else:
                split_polygons = split(polygon, obb_midline2)

            for poly in split_polygons.geoms:
                intersection = polygon.intersection(poly)
                intersection_area = intersection.area
                soll_area = intersection_area/polygon_area * object['properties'][fieldname_scaled_area]
                centroid = intersection.centroid
                centroids.append((centroid.coords[0][0], centroid.coords[0][1], soll_area, intersection, object['properties'][fieldname_portion_of_country]))
        else:
            centroid = object['geometry'].centroid
            centroids.append((centroid.coords[0][0], centroid.coords[0][1], object['properties'][fieldname_scaled_area], object['geometry'], object['properties'][fieldname_portion_of_country]))

    print("Zentroide wurden bestimmt! ...")

    return(centroids)

def generate_quadtree(data, points, max_depth, max_items):
    """
    Erstellt einen Quadtree und füllt ihn mit Datenpunkten. Der Quadtree überdeckt alle Datenpunkte.

    Parameters:
    - data (List): Eine Liste von Objekten, die geometrische Informationen (als Shapely-Polygone) und weitere Eigenschaften enthalten.
    - points (List): Eine Liste von Punkten, die in den Quadtree eingefügt werden sollen. Jeder Punkt muss als Tupel (x, y) vorliegen.
    - max_depth (Integer): Die maximale Tiefe des Quadtree.
    - max_items (Integer): Die maximale Anzahl von Elementen in einem Quadtree-Knoten.

    Returns:
    Index: Quadtree-Index entsprechender Klasse aus pyqtree.
    List:  Sortierten Liste (absteigend nach Flächengröße) von Vierecken (Polygons), die die Quadtree-Leafs repräsentieren.

    Hinweise:
    - Die Quadtree-Struktur kann mit dem zurückgegebenen Quadtree-Objekt weiter untersucht und durchsucht werden.
    - Die sortierte Liste der Polygone kann für die Darstellung oder Analyse der Quadtree-Blätter verwendet werden.

    Beispiel:
    - data = [
            {
                'geometry': <polygon1>,
                'properties': {...}
            },
            {
                'geometry': <polygon2>,
                'properties': {...}
            }
            ...
        ]

    - points = [(x1, y1), (x2, y2), ...]
    """
    print("Quadtree wird erstellt und befüllt!...")

    polygons = []
    for date in data:
        polygons.append(date['geometry'])
    vereinigungs_polygon = unary_union(polygons)
    bb = vereinigungs_polygon.envelope
    coords = mapping(bb)['coordinates']

    quadtree = Index((coords[0][0][0]-1, coords[0][0][1]-1, coords[0][2][0]+1, coords[0][2][1]+1), max_items = max_items, max_depth = max_depth) # "+1" bzw "-1" Damit Punkte trotz Precision-Errors auf jeden Fall innerhalb der Indexstruktur liegen

    for point in points:
        quadtree.insert(point, (point[0], point[1], point[0], point[1]))

    quads = []
    i=1
    for obj in quadtree:
        if not obj.children:
            i += 1
            x_min = obj.center[0] - obj.width/2
            x_max = obj.center[0] + obj.width/2
            y_min = obj.center[1] - obj.height/2
            y_max = obj.center[1] + obj.height/2
            quads.append(Polygon([(x_min, y_min), (x_min, y_max), (x_max, y_max), (x_max, y_min)]))

    sorted_quads = sorted(quads, key=lambda poly: poly.area, reverse=True)

    return(quadtree, sorted_quads)

def add_intersection_points_with_quads(data, quads):
    """
    Bestimmt zusätzliche Punkte zur Erhaltung der Topologie durch den Schnitt von Polygonen mit Quadraten.

    Parameters:
        data (List): Eine Liste von Objekten, die geometrische Informationen (Shapely-Polygons) und weitere Informationen enthalten.
        quads (List): Eine Liste von Quads (Shapely-Polygons), mit denen die Schnittpunkte bestimmt werden sollen.

    Returns:
        List: Liste von zusätzlichen Punkten, die durch den Schnitt der Polygone mit den Quadraten erzeugt wurden. Jeder Punkt besteht aus einem Tupel der Koordnaten in der Form (x, y)

    Hinweise:
    - Die Funktion ermittelt Schnittpunkte zwischen den äußeren Umgrenzungslinien der Polygone und den äußeren Umgrenzungslinien der Quads.
    - Der Schnittpunkt wird auf ganze Koordinaten gerundet, um eine mögliche Redundanz von Punkten zu erreichen.

    Beispiel:
    - data = [
            {
                'geometry': <polygon1>,
                'properties': {...}
            },
            {
                'geometry': <polygon2>,
                'properties': {...}
            }
            ...
        ]

    - quads = [<quad_polygon1>, <quad_polygon2>, ...]
    """
    print("Zusätzliche Punkte zur Topologieerhaltung werden bestimmt! ...")

    intersection_points = []
    for date in data:

        poly = date['geometry']
        poly_exterior_linestring = LineString(poly.exterior.coords)
        for quad in quads:
            quad_exterior_linestring = LineString(quad.exterior.coords)
            if poly_exterior_linestring.intersects(quad_exterior_linestring):
                intersection_possible = True
                try:
                    intersection = poly_exterior_linestring.intersection(quad_exterior_linestring)
                except Exception as e:
                    intersection_possible = False
                    print("Intersection nicht möglich! Exception: ", e)
                if intersection_possible and intersection.geom_type == "MultiPoint":
                    for point in intersection.geoms:
                        coords = point.coords[0]
                        coords_rounded = (round(coords[0]), round(coords[1]))
                        if coords_rounded not in intersection_points:
                            intersection_points.append(coords_rounded)
    return intersection_points

def densify_polygon_points(points, data, intersection_points):
    """
    Verdichtet die Punkte in Polygonen durch Hinzufügen von zusätzlichen Punkten an den Schnittpunkten:
    1. Jedes Polygon wird in Polygon-Shell und Polygon-Hole aufgeteilt. Die Verknüpfung bleibt separat vorhanden.
    2. Die Funktion durchläuft die Schnittpunkte und überprüft, ob ein Punkt mit einer Polygon-Shell schneidet.
    3. Jedem Punkt wird nun eine Position entsprechend des nächstgelegenen Segmentes der Shell ODER des Holes(!) zugeordnet.
    4. Die Punkte in den Koordinatenlisten werden entsprechend ihrer Position sortiert.
    5. Nach dem Hinzufügen der neuen Punkte werden die aktualisierten Polygon-Shells zurückgegeben.

    Parameters:
        points (Dictionary): Ein Dictionary, das die Koordinaten der Punkte, sowie der Positionen in den Polygons enthält.
        data (List): Eine Liste von Objekten, die geometrische Informationen (Shapely-Polygons) und weitere Informationen enthalten.
        intersection_points (List): Eine Liste von Schnittpunkten, die zum Hinzufügen von Punkten verwendet werden sollen.

    Returns:
        List: Eine Liste von Punkten, die aus den verdichteten Polygonen extrahiert wurden.

    Beispiel:
    - points = {
            (x1, y1): [(poly_id1, position1), (poly_id2, position2)],
            (x2, y2): [(poly_id3, position3)]
        }

    - data = [
            {
                'geometry': <polygon1>,
                'properties': {...}
            },
            {
                'geometry': <polygon2>,
                'properties': {...}
            }
            ...
        ]

    - intersection_points = [(x1, y1), (x2, y2), ...]

    - densify_polygon_points(points, data, intersection_points) gibt zurück:
        {
            (x1, y1): [(country_ID1, [position1, position2, ...]), (country_ID2, [position1, position2, ...]), ...],
            (x2, y2): [(country_ID3, [position1, position2, ...]), ...],
            ...
        }
    """

    data_copy = data.copy()
    new_positions = {}
    shells = {}
    for date in data_copy:
        poly = date['geometry']
        shell = Polygon(poly.exterior.coords[:])
        shells[shell.wkt] = [] #holes
        if len(poly.interiors) >= 1:
            for i in range(len(poly.interiors)):
                hole = Polygon(poly.interiors[i])
                shells[shell.wkt].append(hole)
    for point in intersection_points:
        p = Point(point)
        has_intersection = False
        
        for date in data_copy:
            poly = date['geometry']
            shell = Polygon(poly.exterior.coords[:])
            if p.intersects(shell):
                poly_id = date['id']
                nearest_segment = None
                min_distance = float('inf')
                for i in range(len(poly.exterior.coords) - 1):
                    segment_start = poly.exterior.coords[i]
                    segment_end = poly.exterior.coords[i + 1]

                    # Berechne den Abstand des Punktes zur Kante
                    seg = LineString([segment_start, segment_end])
                    distance = p.distance(seg)

                    # Aktualisiere das nächstgelegene Segment, falls der Abstand kleiner ist
                    if distance < min_distance:
                        min_distance = distance
                        nearest_segment = (segment_start, segment_end)
                for hole in shells[shell.wkt]:
                    for i in range(len(hole.exterior.coords)-1):
                        segment_start = hole.exterior.coords[i]
                        segment_end = hole.exterior.coords[i + 1]

                        # Berechne den Abstand des Punktes zur Kante
                        seg = LineString([segment_start, segment_end])
                        distance = p.distance(seg)

                        # Aktualisiere das nächstgelegene Segment, falls der Abstand kleiner ist
                        if distance < min_distance:
                            min_distance = distance
                            nearest_segment = (segment_start, segment_end)

                # Herausfinden ob ein, und wenn, welches Polygon benachbart ist
                neighbor_poly_id = None
                for id_tupel1 in points[nearest_segment[0]]:
                    for id_tupel2 in points[nearest_segment[1]]:
                        if str(id_tupel1[0])!= str(poly_id) and id_tupel1[0] == id_tupel2[0]:
                            neighbor_poly_id = id_tupel1[0]
                        else:
                            pass
                if neighbor_poly_id == None:
                    pass

                position1_in_poly1 = None
                position1_in_poly2 = None
                position2_in_poly1 = None
                position2_in_poly2 = None

                for id_tuple1 in points[nearest_segment[0]]:
                    if id_tuple1[0] == poly_id:
                        position1_in_poly1 = id_tuple1[1]
                    elif id_tuple1[0] == neighbor_poly_id:
                        position1_in_poly2 = id_tuple1[1]
                    else:
                        pass
                
                for id_tuple2 in points[nearest_segment[1]]:
                    if id_tuple2[0] == poly_id:
                        position2_in_poly1 = id_tuple2[1]
                    elif id_tuple2[0] == neighbor_poly_id:
                        position2_in_poly2 = id_tuple2[1]
                    else:
                        pass

                new_position_in_poly1 = None
                new_position_in_poly2 = None

                if position1_in_poly1 != None:
                    min_position1_poly1 = min(position1_in_poly1)
                    max_position1_poly1 = max(position1_in_poly1)
                    min_position2_poly1 = min(position2_in_poly1)
                    max_position2_poly1 = max(position2_in_poly1)

                    if min_position1_poly1 == 0 or min_position2_poly1 == 0:
                        #0 enthalten
                        if min_position1_poly1 == 0 and min_position2_poly1 == 1:
                            #Position 1
                            new_position_in_poly1 = 1
                        elif min_position1_poly1 == 0 and min_position2_poly1 != 1:
                            #Nach Max-Position 2
                            new_position_in_poly1 = max_position2_poly1 + 1
                        elif min_position2_poly1 == 0 and min_position1_poly1 == 1:
                            #Position 1"
                            new_position_in_poly1 = 1
                        elif min_position2_poly1 == 0 and min_position1_poly1 != 1:
                            #Nach Max-Position 1
                            new_position_in_poly1 = max_position1_poly1 + 1
                        else:
                            print("Irgendetwas ist bei der Positionsbestimmung schiefgelaufen1! Poly1")
                    else:
                        #Keine 0 enthalten
                        if min_position1_poly1 > max_position2_poly1:
                            new_position_in_poly1 = max_position2_poly1 + 1
                        elif min_position2_poly1 > max_position1_poly1:
                            new_position_in_poly1 = max_position1_poly1 + 1
                        else:
                            print("Irgendetwas ist bei der Positionsbestimmung schiefgelaufen2 Poly1!")
                else:
                    #print("position1_in_poly1 == None")
                    pass

                if position1_in_poly2 != None:
                    min_position1_poly2 = min(position1_in_poly2)
                    max_position1_poly2 = max(position1_in_poly2)
                    min_position2_poly2 = min(position2_in_poly2)
                    max_position2_poly2 = max(position2_in_poly2)

                    if min_position1_poly2 == 0 or min_position2_poly2 == 0:
                        #0 enthalten
                        if min_position1_poly2 == 0 and min_position2_poly2 == 1:
                            #Position 1
                            new_position_in_poly2 = 1
                        elif min_position1_poly2 == 0 and min_position2_poly2 != 1:
                            #Nach Max-Position 2
                            new_position_in_poly2 = max_position2_poly2 + 1
                        elif min_position2_poly2 == 0 and min_position1_poly2 == 1:
                            #Position 1
                            new_position_in_poly2 = 1
                        elif min_position2_poly2 == 0 and min_position1_poly2 != 1:
                            #Nach Max-Position 1
                            new_position_in_poly2 = max_position1_poly2 + 1
                        else:
                            print("Irgendetwas ist bei der Positionsbestimmung schiefgelaufen1! Poly2")
                    else:
                        #Keine 0 enthalten
                        if min_position1_poly2 > max_position2_poly2:
                            new_position_in_poly2 = max_position2_poly2 + 1
                        elif min_position2_poly2 > max_position1_poly2:
                            new_position_in_poly2 = max_position1_poly2 + 1
                        else:
                            print("Irgendetwas ist bei der Positionsbestimmung schiefgelaufen2! Poly2")
                else:
                    pass
                #POSITIONEN KÖNNEN MEHRFACH SEIN, FALLS MEHRERE PUNKTE AUF EINEM LINIENSEGMENT LIEGEN
                if new_position_in_poly1 != None and str(poly_id) not in list(new_positions.keys()):
                    new_positions[str(poly_id)] = [(new_position_in_poly1, p.coords[0])]
                elif new_position_in_poly1 != None:
                    new_positions[str(poly_id)].append((new_position_in_poly1, p.coords[0]))
                else:
                    pass

                if new_position_in_poly2 != None and str(neighbor_poly_id) not in list(new_positions.keys()):
                    new_positions[str(neighbor_poly_id)] = [(new_position_in_poly2, p.coords[0])]
                elif new_position_in_poly2 != None:
                    new_positions[str(neighbor_poly_id)].append((new_position_in_poly2, p.coords[0]))
                else:
                    pass

                has_intersection == True
                break
        if not has_intersection:
            pass
    
    for poly in new_positions:
        new_positions[poly].sort(key=lambda tup: tup[0])

    for date in data_copy:
        coordinate_list = list(date['geometry'].exterior.coords)
        poly_id = str(date['id'])
        sum_inserted_points = 0

        if poly_id in list(new_positions.keys()):
            same_position = [new_positions[poly_id][0][1]]

            for i in range(len(new_positions[poly_id]) - 1):
                if new_positions[poly_id][i][0] == new_positions[poly_id][i+1][0]:
                    same_position.append(new_positions[poly_id][i+1][1])
                    continue
                else:
                    insert_position = new_positions[poly_id][i][0] + sum_inserted_points

                    if len(same_position) == 1:
                        coordinate_list.insert(insert_position, same_position[0])
                        sum_inserted_points += 1
                    else:
                        point_before = coordinate_list[insert_position - 1]
                        anz_points_inserted = 0
                        while same_position:
                            min_distance = float('inf')
                            for point in same_position:
                                diff = m.sqrt((point_before[0] - point[0])**2)
                                if diff < min_distance:
                                    nearest = point
                                    min_distance = diff
                            coordinate_list.insert(insert_position + anz_points_inserted, nearest)
                            point_before = nearest
                            same_position.remove(nearest)
                            anz_points_inserted += 1
                        sum_inserted_points += anz_points_inserted
                    same_position = [new_positions[poly_id][i+1][1]]

            # Nach der For-Schleife werden ein letztes mal Punkte eingefügt
            insert_position = new_positions[poly_id][-1][0] + sum_inserted_points
            if len(same_position) == 1:
                coordinate_list.insert(insert_position, same_position[0])
                sum_inserted_points += 1
            else:
                point_before = coordinate_list[insert_position - 1]
                anz_points_inserted = 0
                while same_position:
                    min_distance = float('inf')
                    for point in same_position:
                        diff = m.sqrt((point_before[0] - point[0])**2)
                        if diff < min_distance:
                            nearest = point
                            min_distance = diff
                    coordinate_list.insert(insert_position + anz_points_inserted, nearest)
                    point_before = nearest
                    same_position.remove(nearest)
                    anz_points_inserted += 1
                sum_inserted_points += anz_points_inserted

            new_polygon = Polygon(coordinate_list)
            date['geometry'] = new_polygon

    return_points = extract_points(data_copy)
    return return_points

def define_and_apply_force_field(centroids, points, quads, intersection_points, data, enlarge=1, shrink=1, anz_points=4):
    """
    Bestimmt ein Force-Field, berechnet Elastizitätskoeffizienten und wendet das Force-Field auf die Quad-Koordinaten an. Im Anschluss werden die Punkte aus points (plus der Schnittpunkte mit den orignialen äußeren Quad-Umgrenzungs-LineString's) innerhalb der entsprechenden, transformierten Quads interpoliert.

    Parameters:
        centroids (List): Eine Liste aus Tupeln für jedes Objekt (Shapely-Polygon) mit Zentroid-X-Koordinate, Zentroid-X-Koordinate, Skalierte Fläche und der Geometrie des (Shapely)Polygons selbst, .
        points (Dictionary): Ein Dictionary, das die Koordinaten der Punkte, sowie der Positionen in den Polygons enthält.
        quads (List): Eine Liste von Quads (Shapely-Polygons), auf die das Force-Field angewendet werden soll.
        intersection_points (List): Eine Liste von Schnittpunkten, die für die Berechnung des Force-Fields verwendet werden sollen. Die Liste enthält Tupel in der Form (x, y).
        data (List): Eine Liste von Objekten, die geometrische Informationen (Shapely-Polygons) und weitere Informationen enthalten.
        enlarge (Float): Ein Faktor, der bei der Vergrößerung der Flächen angewandt wird und diese verstärkt.
        shrink (Float): Ein Faktor, der bei der Verkleinerung der Flächen angewandt wirdund diese verstärkt.
        anz_points (Int): Anzahl Punkte je approximierenden Kreises. Sie sind Anfangspunkte von Vektoren, die in ihrer Gesamtheit ein Vektorfeld bilden.

    Returns:
        Dictionary: Ein Dictionary mit den aktualisierten Punkt-Koordinaten sowie die Positionen innerhalb der Koordinatenliste entsprechender Polygone .
        List: Eine Liste von Quads (Shapely-Polygons) mit den aktualisierten Koordinaten.

    Hinweise:
    - Das Force-Field wird wesentlich über die Daten in centroids definiert. Punkte auf Kreisen mit dem Zentroid des jeweiligin Polygons als Mittelpunkt und dem aktuellen Flächeninhalt des Polygons streben auf einen Kreis mit gleichem Mittelpunkt, aber skaliertem Flächeninhalt
    - Ist enlarge>shrink, wirkt das Ergebnis eher "aufgebläht". Andersherum eher "schlank"
    - Die Funktion verwendet Multiprocessing, um die Berechnungen parallel durchzuführen.

    Beispiel:
    - centroids = [(x1, y1, area1, geometry1, portion_of_country]), (x2, y2, area2, geometry2, portion_of_country), ...]
    - points = {
            (x1, y1): [(poly_id1, position1), (poly_id2, position2)],
            (x2, y2): [(poly_id3, position3)]
        }
    - quads = [<quad1>, <quad2>, ...]
    - intersection_points = [(x1, y1), (x2, y2), ...]
    - data = [
            {
                'geometry': <polygon1>,
                'properties': {...}
            },
            {
                'geometry': <polygon2>,
                'properties': {...}
            }
            ...
        ]

    new_points, new_quads = define_and_apply_force_field(centroids, points, quads, intersection_points, data, enlarge, shrink)

    Ausgabe der aktualisierten Punkt-Koordinaten in der Form
    - points = {
            (x1, y1): [(poly_id1, position1), (poly_id2, position2)],
            (x2, y2): [(poly_id3, position3)]
        }

    Ausgabe der aktualisierten Quad-Polygone in der Form
    - new_quads = [<quad_polygon1>, <quad_polygon2>, ...]
    """

    force_field_func = None

    print("Force-Field wird bestimmt!...")
    for centroid in centroids:
        portion_of_country = centroid[4]
        ist_area = centroid[3].area
        ist_radius = m.sqrt(ist_area/m.pi) #Durchschnittlicher Radius des Polygon-approximierenden Kreises
        soll_area = centroid[2]
        anz_points = anz_points 
        phi = 2*m.pi/anz_points
        for i in range(anz_points):
            x_pi = centroid[0] + ist_radius * m.cos(i*phi)
            y_pi = centroid[1] + ist_radius * m.sin(i*phi)
            force_func = force_function(x_pi, y_pi, centroid[0], centroid[1], ist_area, soll_area, enlarge, shrink)

            if force_field_func == None:
                force_field_func = force_func
            else:
                force_field_func = add_summand(force_field_func, force_func)


    print("Elastizitätskoeffizienten werden bestimmt!...")

    """Elasitzitätskoeefizienten aus Quad-Punkten"""
    list_of_local_c_values = []
    
    quad_point_list = []
    for quad in quads:
        quad_points = mapping(quad)['coordinates'][0]
        for point in quad_points:
            if point not in quad_point_list:
                quad_point_list.append(point)

    print("Anzahl Quad-Points: ", len(quad_point_list))
    print("Anzahl Intersection-Points: ", len(intersection_points))

    all_points_list = quad_point_list + intersection_points
    arguments = [(p, force_field_func) for p in all_points_list]

    multi_core_start_time = time.time()
 
    num_cores = multiprocessing.cpu_count()
    pool = multiprocessing.Pool(processes=num_cores, initializer=init_worker)
    
    try:
        list_of_local_c_values = pool.starmap(calc_local_elasticity_coefficients, arguments)
    except KeyboardInterrupt:
        pool.terminate()
        pool.join()

    pool.close()
    pool.join()
    multi_core_end_time = time.time()
    multi_core_time = multi_core_end_time - multi_core_start_time
    print("Elastizitätskoeffizienten-Zeit: ", multi_core_time)

    c_global = min(list_of_local_c_values)

    print("Neue Quadkoordinaten werden berechnet !...")
    new_quad_coordinates_start = time.time()
    num_cores = multiprocessing.cpu_count()
    pool = multiprocessing.Pool(processes=num_cores, initializer=init_worker)
    arguments = [(q, force_field_func, c_global) for q in quads]
    
    
    try:
        new_quads = pool.starmap(calc_new_coordinates, arguments) #calc_new_coordinates(quad, force_field_func, c_global)
    except KeyboardInterrupt:
        pool.terminate()
        pool.join()

    pool.close()
    pool.join()

    new_quad_coordinates_end = time.time()
    new_quad_coordinates_time = new_quad_coordinates_end - new_quad_coordinates_start
    print("Quad-Koordinatenberechnung Zeit: ", new_quad_coordinates_time)
    print("Neue Punktkoordinaten werden berechnet! ...")

    num_cores = multiprocessing.cpu_count()
    pool = multiprocessing.Pool(processes=num_cores, initializer=init_worker)
    start = time.time()
    densified_points = densify_polygon_points(points, data, intersection_points)
    end = time.time()
    duration = end - start
    print(f"Schnittpunkte mit Quad-Boundaries wurden den Polygonen hinzugefügt! ...")
    print(f"Verdichtungs-Zeit: {duration}")
    densified_points_list = list(densified_points.items())
    arguments = [(point, quads, new_quads) for point in densified_points_list]
    try:
        list_of_new_points = pool.starmap(interpolate_point_in_transformed_quad, arguments)
    except KeyboardInterrupt:
        pool.terminate()
        pool.join()

    pool.close()
    pool.join()
    new_points = dict(list_of_new_points)
    print("Neue Punktkoordinaten wurden interpoliert!...")

    return new_points, new_quads

def interpolate_point_in_transformed_quad(point, quads, new_quads):
    """
    Interpoliert einen Punkt innerhalb eines transformierten Quads.

    Parameters:
        point (Tupel): Ein Tupel, das die Koordinaten des zu interpolierenden Punktes und die Position in der Koordinatenliste von Polygonen repräsentiert. Erstes Element ist ein Tupel in der Form (x, y). Zweites Element eine Liste in der Form [('polygonID1', [position1, position2]), ('polygonID2', [position1]), ...].
        quads (List): Eine Liste von originalen Quads (Shapely-Polygons), die nach absteigender Fläche sortiert sind.
        new_quads (List): Eine Liste von entsprechenden neuen Quads (Shapely-Polygons), die sich aus der Transformation ergeben.

    Returns:
        Tuple: Ein Tupel, das die interpolierten Koordinaten des Punkts als Tupel und eine Liste von Tupel aus PolygonID und Positionen enthält.
    
    Hinweise:
    - Wenn ein Punkt mit keinem originalen Quad-Polygon intersected, wird ((0, 0), []) zurückgegeben und eine Meldung ausgegeben.

    Beispiel:
    - point =
        ((570881.988, 5925294.98), [('71', [489]), ('77', [293])])
    - quads = [<quad_polygon1>, <quad_polygon2>, ...]
    - new_quads = [<quad_polygon1>, <quad_polygon2>, ...]

    - interpolate_point_in_transformed_quad(point, quads, new_quads) gibt zurück:
        ((572341.988, 5922123.98), [('71', [489]), ('77', [293])])
    """
    shapely_point = Point(point[0])
    intersection_bool = False
    for index, quad in enumerate(quads): # An dieser Stelle ist es wichtig, dass quads der Fläche nach sortiert ist (absteigend)
        if shapely_point.intersects(quad):
            intersection_bool = True
            quad_coords = quad.exterior.coords[:]
            x_quad_coords = [p[0] for p in quad_coords]
            y_quad_coords = [p[1] for p in quad_coords]

            new_quad_coords = new_quads[index].exterior.coords[:]
            x_new_quad_coords = [p[0] for p in new_quad_coords]
            y_new_quad_coords = [p[1] for p in new_quad_coords]
            f_x = LinearNDInterpolator(list(zip(x_quad_coords[:-1], y_quad_coords[:-1])), x_new_quad_coords[:-1])
            f_y = LinearNDInterpolator(list(zip(x_quad_coords[:-1], y_quad_coords[:-1])), y_new_quad_coords[:-1])
            #f_x = CloughTocher2DInterpolator(list(zip(x_quad_coords[:-1], y_quad_coords[:-1])), x_new_quad_coords[:-1])
            #f_y = CloughTocher2DInterpolator(list(zip(x_quad_coords[:-1], y_quad_coords[:-1])), y_new_quad_coords[:-1])

            new_x = f_x([point[0][0], point[0][1]]) 
            new_y = f_y([point[0][0], point[0][1]])
            new_point = ((new_x[0], new_y[0]), point[1])
            break
    if not intersection_bool:
        print("Point Intersected nicht")
        return ((0, 0), [])
    else:
        #print("NEW_POINT: ", new_point)
        return new_point

def rebuild_polygons_from_points(points, old_polygons):
    """Ersetzt die Geometrie alter Polygone aus old_polygons. Die Punkte aus points sind mit den ID's der entsprechenden Polygone assoziiert und mit den entsprechenden Positionen innerhalb der Koordinatenliste versehen.
    Aus diesen Koordinatenlisten können nun erneut Shapely-Geometry-Objekte erzeugt werden.
    Während zunächst jedes Polygon lediglich aus einer Shell besteht, werden die Holes mithilfe des Methodenaufrufes eliminate_containing_polygons_in_list() wiederhergestellt.
    
    Parameters:
        points (Dictionary): Ein Dictionary, das die Koordinaten der Punkte, sowie der Positionen in den Polygons enthält.
        old_polygons (Liste): Eine Liste von Objekten, die geometrische Informationen (als Shapely-Polygone) und weitere Eigenschaften enthalten.

    Returns:
        Dictionary: Eine Liste von Objekten, die geometrische Informationen (als Shapely-Polygone) und weitere Eigenschaften enthalten.

    Beispiel:
    - points = {
            (x1, y1): [(poly_id1, position1), (poly_id2, position2)],
            (x2, y2): [(poly_id3, position3)],
            ...
        }
    - old_polygons = [
        {
            'id': 'polygon1',
            'geometry': {
                'type': 'Polygon',
                'coordinates': [
                    [(0, 0), (0, 5), (5, 5), (5, 0), ..., (0, 0)]
                ]
            }
        },
        {
            'id': 'polygon2',
            'geometry': {
                'type': 'Polygon',
                'coordinates': [
                    [(2, 2), (2, 4), (4, 4), (4, 2), ..., (2, 2)]
                ]
            }
        },
        ...
        ]
    - rebuild_polygons_from_points(points, old_polygons) gibt Dictionary wie old_polygons aus.
        
    """
    polygons = old_polygons.copy()
    
    poly_points = {}
    for point in points:
        for polygon_ids in points[point]:
            id = polygon_ids[0]
            positions = polygon_ids[1]
            if id not in poly_points:
                poly_points[id] = []
            for position in positions:
                while len(poly_points[id]) <= position:
                    poly_points[id].append(None)
                poly_points[id][position] = point

    for polygon in polygons:
        try:
            coordinate_list = poly_points[polygon['id']]
            p = Polygon(coordinate_list)
            if p.is_valid == False:
                max_area = 0
                valid_polygon = None
                geom_collection = make_valid(p)
                if geom_collection.geom_type == "Polygon":
                    valid_polygon = geom_collection
                else:
                    for geom in geom_collection.geoms:
                        if geom.geom_type == "Polygon":
                            if geom.area > max_area:
                                max_area = geom.area
                                valid_polygon = geom
                        elif geom.geom_type == "MultiPolygon":
                            for geom2 in geom.geoms:
                                if geom2.geom_type == "Polygon":
                                    if geom2.area > max_area:
                                        max_area = geom2.area
                                        valid_polygon = geom2
                polygon['geometry'] = valid_polygon
            else:
                polygon['geometry'] = p
        except Exception as e:
            print("")
            print("Exception: ", e)
            print("ERROR: Nicht alle Polygone konnten wieder zusammengesetzt werden")
            coordinate_list = poly_points[polygon['id']]
            clean_coordinate_list = [x for x in coordinate_list if x != None]
            p = Polygon(clean_coordinate_list)
            if p.is_valid == False:
                max_area = 0
                valid_polygon = None
                geom_collection = make_valid(p)
                if geom_collection.geom_type == "Polygon":
                    valid_polygon = geom_collection
                else:
                    for geom in geom_collection.geoms:
                        if geom.geom_type == "Polygon":
                            if geom.area > max_area:
                                max_area = geom.area
                                valid_polygon = geom
                polygon['geometry'] = valid_polygon
            else:
                polygon['geometry'] = p

    print("Polygone wurden generiert! ...")
    for polygon in polygons:
        if isinstance(polygon["geometry"], base.BaseGeometry) == False:
            print(dict(polygon))
    polygons = eliminate_containing_polygons_in_list(polygons)
    return(polygons)

def eliminate_containing_polygons_in_list(polygons):
    """
    Polygone, die vollständig innenerhalb eines anderen Polygons liegen (DE-9IM: "212FF1FF2") werden dem anderen Polygon als Hole hinzugefügt.

    Parameters:
        polygons (List): List of dict-like Objects with Shapely-Polygons as geometry in polygon['geometry']
    
    Returns:
        polygons_copy (List): List of dict-like Objects with new Shapely-Polygons as geometry in polygon['geometry']
    """
    polygons_copy = polygons.copy()

    for poly1 in polygons_copy:
        for poly2 in polygons_copy:
            if poly1['geometry'].relate_pattern(poly2['geometry'], "212FF1FF2"):
                if poly1['geometry'].geom_type == "Polygon":
                    new_interiors = list(poly1['geometry'].interiors)
                    new_interiors.append(poly2['geometry'].exterior)
                    new_polygon = Polygon(shell = poly1['geometry'].exterior, holes = new_interiors)
                    new_polygon = make_valid(new_polygon)
                    poly1['geometry'] = new_polygon
    print("Eventuelle Holes wurden wiederhergestellt! ...")
    return(polygons_copy)

def calculate_errors(actual_polygons, schema, related_partial_polygons, input_polygons, area_field="Scaled", rmsre_field="RMSRE", wme_keim_field="WME_Keim", wme_field="WME"):
    """
    Berechnet den Root-Mean-Square-Relative-Error (RMSRE) der Flächengrößen, sowie eine flächengewichtete Fehlermetrik (WME) nacht Keim et. al (2004).
    Zusammengehörige Teil-Polygone, die durch related_partial_polygons verknüpft sind, 
    werden vor der RMSRE-Berechnung in ihren IST-, und SOLL-Flächengrößen zusammengefasst.

    Parameters:
        actual_polygons (Dictionary):  Dictionary mit den Schlüsseln 'geometry', 'id' und 'properties'. 
                                    area_field muss dabei als Schlüssel in 'properties' enthalten sein
        schema (Dictionary):        Schema für das Speichern in einer ESRI-Shape-Datei. 
                                    Wird um ein zusätzliches Feld rmsre_field und dem Datentyp 'float' erweitert.
        area_field (String):        Feldname in dem die SOLL-Flächen enthalten sind.
                                    Default: "Scaled"
        rmsre_field (String):        Feldname in dem der RMSRE gespeichert wird. Ist für alle Objekte gleich (globales Fehlermaß).
                                    Default: "RMSRE"
        wme_field (String):        Feldname in dem der Weighted Mean Error (WME) nach Keim et. al (2004) gespeichert wird. Ist für alle Objekte gleich (globales Fehlermaß).
                                    Default: "WME"

    Returns:
        new_polygons (Dictionary):  Erweitertes Dictionary der Eingabe actual_polygons. 
                                    Der RMSRE wurde in 'properties' mit dem Schlüssel rmsre_field hinzugefügt.
        schema (Dictionary):        Erweitertes Dictionary der Eingabe schema.
                                    Wird um ein zusätzliches Feld rmsre_field und dem Datentyp 'float' erweitert.
        rmsre (Float):               Root-Mean-Square-Error (RMSRE)
        wme_keim (Float):            Weighted Mean Error (WME) nach Keim et. al (2004)
    """

    print("Fehler-Metriken werden berechnet! ...")
    ist_field = "ist_flaeche"
    soll_field = "soll_flaeche"
    error_field = "error"
    keim_error_field = "keim_error"
    max_error_field = "max_error"
    max_keim_error_field = "max_keim_error"
    abs_abweichung_field = "abs_abweichung"
    hd_field = "hausdorff_distance"
    mean_hd_field = "mean_hausdorff_distance"
    polygons = list(actual_polygons)
    copy_input_polygons = list(input_polygons)
    anzahl = 0
    squared_error = 0
    actual_polygon_areas = {}
    target_polygon_areas = {}
    ids_already_taken_in_account = []
    actual_total_area = 0
    target_total_area = 0
    wme_keim = 0 # weighted mean keim_error
    wme = 0
    max_error = 0
    max_keim_error = 0
    for polygon in polygons:
        actual_polygon_areas[polygon['id']] = polygon['geometry'].area
        actual_total_area += polygon['geometry'].area
        target_polygon_areas[polygon['id']] = polygon['properties'][area_field]
        target_total_area += polygon['properties'][area_field]
    for polygon in polygons:
        calculation_finished_for_polygon = False
        if polygon['id'] not in ids_already_taken_in_account and isinstance(polygon['geometry'], base.BaseGeometry) and polygon['geometry'].geom_type == "Polygon":
            sum_related_actual_areas = 0
            sum_related_target = 0
            for relations in related_partial_polygons:
                all_related_polygon_ids = [related_id[0] for related_id in relations]
                if polygon['id'] in all_related_polygon_ids:
                    sum_related_actual_areas = sum([actual_polygon_areas[related_id[0]] for related_id in relations])
                    sum_related_target = sum([target_polygon_areas[related_id[0]] for related_id in relations])
                    erwartet = sum_related_target
                    beobachtet = sum_related_actual_areas
                    ids_already_taken_in_account.extend(all_related_polygon_ids)
                    anzahl += 1
                    error = (abs(beobachtet - erwartet) / erwartet)
                    keim_error = abs(erwartet - beobachtet) / (erwartet + beobachtet)
                    squared_error += error**2
                    wme_keim += keim_error*erwartet/target_total_area
                    wme += error*erwartet/target_total_area
                    calculation_finished_for_polygon = True

                    if error > max_error:
                        max_error = error
                    if keim_error > max_keim_error:
                        max_keim_error = keim_error

                    for polygon2 in polygons:
                        if polygon2['id'] in all_related_polygon_ids:
                            polygon2['properties'][ist_field] = beobachtet
                            polygon2['properties'][soll_field] = erwartet
                            polygon2['properties'][error_field] = error
                            polygon2['properties'][keim_error_field] = keim_error
                            polygon2['properties'][abs_abweichung_field] = beobachtet-erwartet
                    break
            if calculation_finished_for_polygon == False:
                erwartet = polygon['properties'][area_field]
                beobachtet = polygon['geometry'].area
                anzahl += 1
                error = (abs(beobachtet - erwartet) / erwartet)
                keim_error = abs(erwartet - beobachtet) / (erwartet + beobachtet)
                squared_error += error**2
                wme_keim += keim_error*erwartet/target_total_area
                wme += error*erwartet/target_total_area
                polygon['properties'][ist_field] = beobachtet
                polygon['properties'][soll_field] = erwartet
                polygon['properties'][error_field] = error
                polygon['properties'][keim_error_field] = keim_error
                polygon['properties'][abs_abweichung_field] = beobachtet-erwartet

                if error > max_error:
                    max_error = error
                if keim_error > max_keim_error:
                    max_keim_error = keim_error

    mse = squared_error / anzahl
    rmsre = m.sqrt(mse)
     
    print("RMSRE: ", rmsre)
    print("Weighted Mean Error (WME_Keim): ", wme_keim)
    print("Weighted Mean Error (WME): ", wme)
    print("Maximal Keim-Error: ", max_keim_error)
    print("Maximal Error: ", max_error)

    for polygon in polygons:
        polygon['properties'][rmsre_field] = rmsre
        polygon['properties'][wme_keim_field] = wme_keim
        polygon['properties'][wme_field] = wme
        polygon['properties'][max_error_field] = max_error
        polygon['properties'][max_keim_error_field] = max_keim_error
    
    print("")
    print("Ähnlichkeitsmaße werden bestimmt! ...")

    sum_hausdorff_distances = 0
    for polygon in polygons:
        for input_polygon in copy_input_polygons:
            if polygon['id'] == input_polygon['id']:
                normalized_polygon, normalized_input_polygon = normalize_polygons_to_unit_area(Polygon(polygon['geometry'].exterior.coords), Polygon(input_polygon['geometry'].exterior.coords))
                hausdorff_distance = calculate_hausdorff_distance(normalized_polygon, normalized_input_polygon)
                polygon['properties'][hd_field] = hausdorff_distance
                sum_hausdorff_distances += hausdorff_distance
                break

    mean_hausdorff_distance = sum_hausdorff_distances/len(polygons)

    for polygon in polygons:
        polygon['properties'][mean_hd_field] = mean_hausdorff_distance

    schema['properties'][rmsre_field] = 'float'
    schema['properties'][wme_field] = 'float'
    schema['properties'][wme_keim_field] = 'float'
    schema['properties'][ist_field] = 'float'
    schema['properties'][soll_field] = 'float'
    schema['properties'][error_field] = 'float'
    schema['properties'][keim_error_field] = 'float'
    schema['properties'][max_keim_error_field] = 'float'
    schema['properties'][max_error_field] = 'float'
    schema['properties'][abs_abweichung_field] = 'float'
    schema['properties'][hd_field] = 'float'
    schema['properties'][mean_hd_field] = 'float'
    return polygons, schema, rmsre, wme_keim, mean_hausdorff_distance

def scale_to_total_area(polygons, area_field="Scaled"):
    """
    Skaliert die einzelnen Polygone so, dass sie in ihrer Summe genau so groß sind wie die Summe der Flächengrößen, wie sie in area_field stehen

    Parameters:
        polygons (Dictionary):  Dictionary mit den Schlüsseln 'geometry', 'id' und 'properties'. 
                                    area_field muss dabei als Schlüssel in 'properties' enthalten sein
        area_field (String):        Feldname in dem die SOLL-Flächen enthalten sind.
                                    Default: "Scaled"

    Returns:
        new_polygons (Dictionary):  Dictionary, was der Eingabe polygons entspricht, wobei jedoch die Geometrie durch die skalierte Geometrie ersetzt wurde.

    """
    desired_total_area = 0
    actual_total_area = 0
    all_geometries = []
    for polygon in polygons:
        desired_total_area += polygon['properties'][area_field]
        actual_total_area += polygon['geometry'].area
        all_geometries.append(polygon['geometry'])
    union_geometry = unary_union(all_geometries)
    center_of_map = union_geometry.centroid
    scaling_factor = m.sqrt(desired_total_area/actual_total_area)
    #print("desired_total_area: ", desired_total_area)
    #print("actual_total_area: ", actual_total_area)
    #print("verhältnis: ", desired_total_area/actual_total_area)
    #print("center_of_map: ", center_of_map.wkt)
    new_polygons = polygons.copy()
    for polygon in new_polygons:
        polygon['geometry'] = scale(geom=polygon['geometry'], xfact=scaling_factor, yfact=scaling_factor, origin=center_of_map)
    print("Gesamtfläche wurde justiert! ...")
    return new_polygons

def normalize_polygons_to_unit_area(poly1, poly2):
    target_area = 1
    area1 = poly1.area
    area2 = poly2.area
    scale_factor1 = (target_area / area1) ** 0.5
    scale_factor2 = (target_area / area2) ** 0.5
    poly1_scaled = scale(poly1, xfact=scale_factor1, yfact=scale_factor1, origin='center')
    poly2_scaled = scale(poly2, xfact=scale_factor2, yfact=scale_factor2, origin='center')

    centroid1 = poly1_scaled.centroid
    centroid2 = poly2_scaled.centroid
    translate_x1, translate_y1 = -centroid1.x, -centroid1.y
    translate_x2, translate_y2 = -centroid2.x, -centroid2.y
    poly1_normalized = translate(poly1_scaled, xoff=translate_x1, yoff=translate_y1)
    poly2_normalized = translate(poly2_scaled, xoff=translate_x2, yoff=translate_y2)

    return poly1_normalized, poly2_normalized

def calculate_hausdorff_distance(poly1, poly2):
    # Calculate the Hausdorff distance between the two polygons
    return poly1.hausdorff_distance(poly2)



if __name__ == "__main__":
    path_shp = PATH_SHP 
    fieldname = FIELD_NAME 
    path_output_shp = PATH_OUTPUT_SHP 
    iterations = MAX_ITERATIONS
    enlarge = ENLARGE
    shrink = SHRINK
    max_depth = MAX_DEPTH
    max_items = MAX_ITEMS
    variable_depth = VARIABLE_DEPTH
    better_than_rmsre = RMSRE_LIMIT
    circle_points = CIRCLE_POINTS
    fieldname2 = FIELD_NAME2
    calculation_method = CALC_METHOD
    faktor = FAKTOR
    exponent = EXPONENT
    total_size_correction = TOTAL_SIZE_CORRECTION

    input_data, related_partial_polygons, schema, crs = input_polygon_shape_file(path_shp)
    original_input_data = copy.deepcopy(input_data)
    deepening_per_iteration = max_depth/iterations

    anzahl_durchgeführter_iterations = 0
    rmsre = None #Root Mean Square Relativ Error
    wme = None #Weightet Mean Error nach Keim et. al (2004)

    for i in range(iterations):
        if variable_depth:
            if iterations == i+1:
                depth = max_depth
            else:
                depth = round(deepening_per_iteration*(i+1), ndigits=None)
        else:
            depth = max_depth
        if depth < 1:
            depth = 1
        print("Durchlauf: ", i+1)
        print("Quadtree-Tiefe: ", depth)
        if i == 0:
            data_with_scale_factor, schema = calculate_scaled_areas(input_data, fieldname, schema, fieldname2, method=calculation_method, faktor=faktor, exponent=exponent, related_partial_polygons=related_partial_polygons)
        else:
            if rmsre<better_than_rmsre:
                print("Qualitäts-Schranke erreicht! ...")
                print("Algorithmus wird beendet! ...")
                break
            data_with_scale_factor, schema = calculate_scaled_areas(new_polygons, fieldname, schema, fieldname2, method=calculation_method, faktor=faktor, exponent=exponent, related_partial_polygons=related_partial_polygons)
        
        points = extract_points(data_with_scale_factor)
        centroids = extract_centroid_with_scaled_area(data_with_scale_factor)
        quadtree, quads = generate_quadtree(data_with_scale_factor, points, depth, max_items)
        intersection_points = add_intersection_points_with_quads(data_with_scale_factor, quads)
        #write_list_to_shape_file(quads, 'Polygon', crs, "quads_last.shp") #<<<--- Hiermit können die Quad-Polygone gespeichert werden (Bitte beachten, dass das Shape-File jede Iteration erneut überschrieben wird)
        new_points, new_quads = define_and_apply_force_field(centroids, points, quads, intersection_points, data_with_scale_factor, enlarge, shrink, circle_points)
        #write_list_to_shape_file(new_quads, 'Polygon', crs, "new_quads_last.shp") #<<<--- Hiermit können die transformierten Quad-Polygone gespeichert werden (Bitte beachten, dass das Shape-File jede Iteration erneut überschrieben wird)
        new_polygons = rebuild_polygons_from_points(new_points, data_with_scale_factor)
        if total_size_correction == True:
            new_polygons = scale_to_total_area(new_polygons, area_field="Scaled")
        new_polygons_with_errors, schema, rmsre, wme_keim, mean_hausdorff_distance = calculate_errors(new_polygons, schema, related_partial_polygons, original_input_data, area_field="Scaled")
        anzahl_durchgeführter_iterations += 1
    print("")
    print("Endbericht!")
    print(f"Anzahl Iterationen: {anzahl_durchgeführter_iterations}")
    print(f"RMSRE-Schranke (Ziel): {better_than_rmsre}")
    print(f"RMSRE (erreicht): {rmsre}")
    print(f"Keim-WME (erreicht): {wme_keim}")
    print(f"Mittlerer Hausdorff-Abstand: {mean_hausdorff_distance}")
    print("")

    schema['properties']['anz_iter'] = 'int'
    for polygon in new_polygons_with_errors:
        polygon['properties']['anz_iter'] = anzahl_durchgeführter_iterations
    write_shape_file(new_polygons_with_errors, schema, crs, path_output_shp)