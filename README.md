# Python Cartogram Algorithm
***Experimentelle Implementation** von Johannes Wolf*



## Der Algorithmus

Der Algorithmus zur Erstellung von Proportionalflächendarstellungen folgt der Logik von [Shipeng Sun](http://sunsp.net/).

Insbesondere folgende Publikationen sind die Grundlage für diese Python-Implementation:
- Sun, S. (2020). Applying forces to generate cartograms: a fast and flexible transformation framework. [Cartography and Geographic Information Science 47(5)](https://doi.org/10.1080/15230406.2020.1745092): 381-399.
- Sun, S. (2013). A Fast, Free-Form Rubber-Sheet Algorithm for Contiguous Area Cartograms. [International Journal of Geographic Information Science 27 (3)](https://doi.org/10.1080/13658816.2012.709247): 567-93.

#### Die Logik in Kurzform:
1. Jedes Eingangspolygon wird durch einen Kreis mit dem Zentroid als Mittelpunkt und dem entsprechenden Flächeninhalt approximiert.
2. Gleichmäßig verteilte Punkte auf den Kreisen dienen als Urbild der Abbildung (Translation definiert durch Vektorfeld) hin zu Kreisen mit gewünschter Polygon-Fläche.
3. Die Stützpunkte der Eingangspolygone werden in eine Quadtree-Struktur eingefügt, sodass ein (unregelmäßiges) Gitter entsteht, welches alle Eingenagspolygone überdeckt.
4. Die Gitterkoordinaten werden transformiert:
    1. Zunächst wird ein maximal großer Faktor bestimmt, der in dem Maße auf die Translation der Gitterpunkte angewandt wird, dass die Topologie gerade noch erhalten bleibt.
    2. Die Gitterkoordinaten werden transformiert.
5. Die Stützpunkte der Eingangspolygone, sowie die Schnittpunkte der Polygone mit dem ursprünglichen Gitter, werden innerhalb der transformierten Gitterzellen interpoliert.

#### Veranschaulichung eines Iterationsschrittes:
![](https://i.ibb.co/vVKWWxR/iteration-step.png "First Iteration")


## Installation

Das Python-Skript nutzt externe Bibliotheken, die zunächst Installiert werden müssen. Nutze hierfür am besten die ```requirements.txt``` oder die ```environment.yml```.

Die wesentlichen Packages sind:
- Numpy
- Fiona
- Shapely
- Pyqtree
- Scipy
- Functools
- Multiprocessing

Vor Ausführung des Skriptes (```cartogramm-algorithm.py```) müssen im Kopf der Datei die Parameter definiert werden. Viel Spaß!

## Beschreibung der Parameter

**Beispielkonfiguration:**
```
PATH_SHP = "data.\Deutschland_Inseln_reduziert.shp" 
FIELD_NAME = "t1" 
FIELD_NAME2 = "t2"
CALC_METHOD = "standardized_relative_change"
FAKTOR = 1
EXPONENT = 1
RMSRE_LIMIT = 0.05
MAX_ITERATIONS = 10
PATH_OUTPUT_SHP = "output.shp"
ENLARGE = 3
SHRINK = 3
CIRCLE_POINTS = 8
MAX_DEPTH = 5
MAX_ITEMS = 1
TOTAL_SIZE_CORRECTION = True
VARIABLE_DEPTH = False
```

#### Beispielergebnis:
![](https://i.ibb.co/xhH6Qnp/result.png "Ten Iterations")

**PATH_SHP** (String)  
Pfad zu einer ESRI-SHAPE Datei mit den Polygonen (z.B. Länder, Bundesländer, Stadtteile o.ä)

**FIELD_NAME** (String)  
Feldname der Datei, die durch PATH_SHP gewählt ist. Das Feld muss für jedes Polygon einen positiven Integer-, oder Float-Wert beinhalten. Je nach Berechnuungsmethode (CALC_METHOD) wird durch den Wert des Feldes die angestrebte Flächengröße des Polygons bestimmt. Sollen Änderungen visualisiert werden, stellt dieser Wert die Referenz für die Änderung dar.

**FIELD_NAME2** (String)
Feldname der Datei, die durch PATH_SHP gewählt ist. Das Feld muss für jedes Polygon einen positiven Integer-, oder Float-Wert beinhalten. Je nach Berechnuungsmethode (CALC_METHOD) wird durch den Wert des Feldes die angestrebte Flächengröße des Polygons bestimmt. Sollen Änderungen visualisiert werden, wird dieser Wert im Verähltnis zum Wert aus FIELD_NAME betrachtet.

**CALC_METHOD** (String)
Mithilfe dieses Parameters wird die Berechnungsmethode für die Ziel-Flächengrößen gewählt. Es gibt momentan drei verschiedene Methoden:
- "standardized_absolute_values": Die Flächengröße ist proportional zu absoluten (positiven) Werten. Die Gesamtfläche bleibt erhalten. Es wird nur der Wert des Feldes "fieldname1" benutzt.
- "standardized_relative_change": Die Flächengrößenänderung entspricht der normierten (auf Gesamtwertänderung) prozentualen Wertänderung. Die Gesamtfläche bleibt erhalten.
- "relative_change": Die Flächengrößenänderung entspricht der prozentualen Wertänderung.

**MAX_ITERATIONS** (Integer)  
Unter anderem die Anzahl an Iterationsschritten ist maßgeblich für die Qualität des Ergebnisses. Eine hohe Zahl führt jedoch gleichzeitig zu einer langen Laufzeit des Programms. Mithilfe des Parameters MAX_ITERATIONS kann eine Abbruch-Bedingung definiert werden, sodass der Algorithmus spätestens nach der eingegebenen Maximalzahl an Iterationen beendet wird.

**RMSRE_LIMIT** (Float)
Mithilfe des Parameters kann eine Abbruch-Bedingung für das Programm definiert werden. Wenn der Root-Mean-Square-Relative-Error (RMSRE) kleiner (besser) als der übergebene Wert ist, wird im Anschluss keine weitere Iteration durchgeführt. Der RMSRE gibt die durchschnittliche relative Abweichung der IST-Flächengrößen von den SOLL-Flächengrößen an.

**PATH_OUTPUT_SHP** (String)  
Pfad der neu erzeugten ESRI-SHAPE Datei. Sie enthält die transformierten Polygone.

**ENLARGE and SHRINK** (Float)  
Faktor zur Gewichtung der Vergrößerung/Verkleinerung der Polygone im Transformationsprozess. Ist ENLARGE > SHRINK wirkt das Ergebnis eher *aufgebläht*, andersherum wirken die Polygone eher *schlank*.

**CIRCLE_POINTS** (Int)
Jedes Polygon wird im Verlauf des Algorithmus durch einen Kreis approximiert. Gleichmäßig verteilte Punkte auf den Kreisen dienen dann als Urbild der Abbildung. Mithilfe des Parameters CIRCLE_POINTS kann die Anzahl der Punkte pro Kreis festgelegt werden.

**MAX_DEPTH** (Integer)  
Maximale Tiefe der Quadtree-Struktur. Der Parameter definiert also zusammen mit MAX_ITEMS die Größe des Gitters für die Transformation. Ein zu niedriger Wert führt dazu, dass durch große Gitterzellen die Sensibilität für kleinräumige Größenänderungen der Polygone nicht vorhanden ist. Ein hoher Wert führt zu langer Laufzeit.

**MAX_ITEMS** (Integer)  
Maximale Itemzahl eines Quads der Quadtree-Struktur. Der Parameter definiert also zusammen mit MAX_DEPTH die Größe des Gitters für die Transformation. Da die Quadtree-Struktur durch Einfügen der Stützpunkte der Eingangspolygone erzeugt wird, führt ein hoher Wert von MAX_ITEMS (lokal) zu einer geringeren Quadtree-Tiefe. Die Auswirkung ist weniger vorhersehbar als von MAX_DEPTH. Aus diesem Grund wird MAX_ITEMS = 1 empfohlen.

**TOTAL_SIZE_CORRECTION** (Boolean)
Bietet mit True die Möglichkeit die Gesamtfläche nach jeder Iteration auf die Soll-Gesamtfläche zu skalieren. Die Abweichungen der einzelnen Flächen von IST zu SOLL kann dadurch verschlechtert werden.

**VARIABLE_DEPTH** (Boolean) - EXPERIMENTELL  
Bietet die Möglichkeit den Parameter MAX_DEPTH für jeden Iterationsschritt anzupassen, sodass in der letzten Iteration der gesetzte Parameter von MAX_DEPTH angewandt wird. Bei jedem vorherigen Schritt wird eine Tiefe proportional zu dem Wert von ITERATIONS angewandt. Das Anwenden dieser Eigenschaft führt nicht unbedingt zu besseren Ergebnissen.





