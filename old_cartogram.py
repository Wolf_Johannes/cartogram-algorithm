import fiona
from shapely.geometry import LineString, Polygon, MultiPolygon, Point, mapping
from shapely.ops import transform, linemerge, unary_union
from shapely.strtree import STRtree
import pyproj
from fiona.crs import from_epsg, from_string
from shapely import wkt
from shapely.validation import make_valid
from shapely import affinity
import math as m
import cmath as cm
from itertools import combinations
from sympy import symbols, sqrt, evalf, diff
import sys
from pyqtree import Index
from scipy.interpolate import interp2d, RegularGridInterpolator
import numpy as np
import time
import itertools
from functools import partial

def force_function(x_pi, y_pi, x_ui, y_ui):
    d_pi_ui = m.sqrt((x_pi-x_ui)**2+(y_pi-y_ui)**2)

    def func(x_q, y_q):
        d_pi_q = sqrt((x_q-x_pi)**2+(y_q-y_pi)**2)
        soll_betrag = d_pi_ui * m.e**(-d_pi_q/d_pi_ui) #d_pi_ui * e^^(-d_pi_q/d_pi_ui)
        ist_betrag = m.sqrt((x_ui-x_pi)**2+(y_ui-y_pi)**2)
        return np.array([(x_ui-x_pi)*(soll_betrag/ist_betrag),(y_ui-y_pi)*(soll_betrag/ist_betrag)])
    
    def zero_func(x_q, y_q):
        return np.array([0,0])
    
    if d_pi_ui != 0:
        #x_q, y_q = symbols('x_q y_q')
        #d_pi_q = sqrt((x_q-x_pi)**2+(y_q-y_pi)**2)
        #soll_betrag = d_pi_ui * m.e**(-d_pi_q/d_pi_ui) #d_pi_ui * e^^(-d_pi_q/d_pi_ui)
        #ist_betrag = m.sqrt((x_ui-x_pi)**2+(y_ui-y_pi)**2)

        force = func#((x_ui-x_pi)*(soll_betrag/ist_betrag),(y_ui-y_pi)*(soll_betrag/ist_betrag))
    else:
        force = zero_func#lambda x_q, y_q: (0,0)zero_func(x_q, y_q)(0, 0)

    return force
"""
def force_function_alternative(x_pi, y_pi, centroid_x, centroid_y, area_ist, area_soll):
    #r_ist = m.sqrt(area_ist/m.pi)
    #r_soll = m.sqrt(area_soll/m.pi)
    r_j = m.sqrt(area_soll/area_ist)
    p_enlarge = 3
    p_shrink = 3
    if r_j  >= 1:
        r_j_e = 1 + p_enlarge*(m.e**(r_j-1)-1)
    elif r_j < 1:
        r_j_e = r_j*(1+p_shrink*m.log(r_j))
    #print("r_j_e: ", r_j_e)
    x_ui = centroid_x + r_j_e * (x_pi - centroid_x)
    y_ui = centroid_y + r_j_e * (y_pi - centroid_y)
    d_pi_ui = m.sqrt((x_pi-x_ui)**2+(y_pi-y_ui)**2)

    def func(x_q, y_q):
        x_ui = centroid_x + r_j_e * (x_pi - centroid_x)
        y_ui = centroid_y + r_j_e * (y_pi - centroid_y)
        d_pi_q = sqrt((x_q-x_pi)**2+(y_q-y_pi)**2)
        soll_betrag = d_pi_ui * m.e**(-d_pi_q/d_pi_ui) #d_pi_ui * e^^(-d_pi_q/d_pi_ui)
        ist_betrag = m.sqrt((x_ui-x_pi)**2+(y_ui-y_pi)**2)
        return np.array([(x_ui-x_pi)*(soll_betrag/ist_betrag),(y_ui-y_pi)*(soll_betrag/ist_betrag)])
    
    def zero_func(x_q, y_q):
        return np.array([0,0])
    
    if d_pi_ui != 0:
        #x_q, y_q = symbols('x_q y_q')
        #d_pi_q = sqrt((x_q-x_pi)**2+(y_q-y_pi)**2)
        #soll_betrag = d_pi_ui * m.e**(-d_pi_q/d_pi_ui) #d_pi_ui * e^^(-d_pi_q/d_pi_ui)
        #ist_betrag = m.sqrt((x_ui-x_pi)**2+(y_ui-y_pi)**2)

        force = func#((x_ui-x_pi)*(soll_betrag/ist_betrag),(y_ui-y_pi)*(soll_betrag/ist_betrag))
    else:
        force = zero_func#lambda x_q, y_q: (0,0)zero_func(x_q, y_q)(0, 0)

    return force"""

from functools import partial

def func(x_q, y_q, centroid_x, centroid_y, x_pi, y_pi, area_ist, area_soll, r_j_e, d_pi_ui):
    x_ui = centroid_x + r_j_e * (x_pi - centroid_x)
    y_ui = centroid_y + r_j_e * (y_pi - centroid_y)
    d_pi_q = sqrt((x_q - x_pi) ** 2 + (y_q - y_pi) ** 2)
    soll_betrag = d_pi_ui * m.e ** (-d_pi_q / d_pi_ui)  # d_pi_ui * e^^(-d_pi_q/d_pi_ui)
    ist_betrag = m.sqrt((x_ui - x_pi) ** 2 + (y_ui - y_pi) ** 2)
    return np.array([(x_ui - x_pi) * (soll_betrag / ist_betrag), (y_ui - y_pi) * (soll_betrag / ist_betrag)])

def zero_func(x_q, y_q):
    return np.array([0, 0])

def force_function_alternative(x_pi, y_pi, centroid_x, centroid_y, area_ist, area_soll):
    r_j = m.sqrt(area_soll / area_ist)
    p_enlarge = 3
    p_shrink = 3
    if r_j >= 1:
        r_j_e = 1 + p_enlarge * (m.e ** (r_j - 1) - 1)
    elif r_j < 1:
        r_j_e = r_j * (1 + p_shrink * m.log(r_j))

    x_ui = centroid_x + r_j_e * (x_pi - centroid_x)
    y_ui = centroid_y + r_j_e * (y_pi - centroid_y)
    d_pi_ui = m.sqrt((x_pi - x_ui) ** 2 + (y_pi - y_ui) ** 2)

    force = partial(func, centroid_x=centroid_x, centroid_y=centroid_y, x_pi=x_pi, y_pi=y_pi,
                    area_ist=area_ist, area_soll=area_soll, r_j_e=r_j_e, d_pi_ui=d_pi_ui)

    zero_force = partial(zero_func)

    if d_pi_ui == 0:
        force = zero_force

    return force
#def add_summand(func1, func2):
    #return lambda x_q, y_q: func1(x_q, y_q) + func2(x_q, y_q)

def add_summand(func1, func2):
    return partial(sum_funcs, func1=func1, func2=func2)

def sum_funcs(x_q, y_q, func1, func2):
    return func1(x_q, y_q) + func2(x_q, y_q)

def diff_x(func, x, y):
    #Ableitung der Funktion func nach x an der Stelle x, y
    h = 1e-6
    return (func(x+h, y)-func(x-h, y))/(2*h)

def diff_y(func, x, y):
    #Ableitung der Funktion func nach y an der Stelle x, y
    h = 1e-6
    return (func(x, y+h)-func(x, y-h))/(2*h)

def calc_new_coordinates(quad, force_field_func, c_global):
    quad_points = mapping(quad)['coordinates'][0]
    point_list = []
    for point in quad_points:
        #values = {x_q: point[0], y_q: point[1]}
        #print("FORCE_FIELD[0]: ", force_field[0].evalf(subs=values))
        #print("FORCE_FIELD[1]: ", force_field[1].evalf(subs=values))
        #print("Point: ", point)
        point_list.append((point[0] + c_global * force_field_func(point[0], point[1])[0],
                            point[1] + c_global * force_field_func(point[0], point[1])[1]))
        #SYMPY
        #point_list.append((point[0] + c_global * force_field[0].evalf(subs=values),
        #point[1] + c_global * force_field[1].evalf(subs=values)))
        
    new_quad = Polygon(point_list)
    return new_quad

def calc_local_elasticity_coefficients(point, force_field_func):
    #global force_field_func
    #print("Multiprozessfunktion wird ausgeführt")
    #print(type(force_field_func))
    #print(point)
    epsilon = sys.float_info.epsilon
    #local_coefficients_list = []
    #print("DEBUG")
    #print(force_field_func)
    #print(point_list)

    x_diff = diff_x(force_field_func, point[0], point[1])
    y_diff = diff_y(force_field_func, point[0], point[1])
    eval_force_x_dx = x_diff[0] #force_x_dx.evalf(subs=values)
    eval_force_y_dx = x_diff[1] #force_y_dx.evalf(subs=values)
    eval_force_x_dy = y_diff[0] #force_x_dy.evalf(subs=values)
    eval_force_y_dy = y_diff[1] #force_y_dy.evalf(subs=values)
    a = eval_force_x_dx * eval_force_y_dy - eval_force_x_dy * eval_force_y_dx
    b = eval_force_x_dx + eval_force_y_dy
    c_local = None

    if a==0 and b<0:
        c_local = (epsilon - 1)/b
    elif a!=0 and b**2>4*a: #*(1-epsilon):
        c_local = (-b-m.sqrt((b**2)-(4*a*(1-epsilon))))/(2*a)
    else:
        c_local = 1
        #print("c_local: ", c_local)
    if c_local < 0 or c_local > 1:
        #print("c_local auf 1 gesetzt")
        c_local = 1
        
    #local_coefficients_list.append(c_local)
    #print("Return: ", c_local)
    return c_local# local_coefficients_list

def input_polygon_shape_file(path):
    raw_input = []
    shapely_input = []
    with fiona.open(path, "r") as src:
        schema = src.schema
        crs = src.crs
        for i in src:
            raw_input.append(i)
            shapely_input.append(i)
            if i['geometry']['type'] == 'Polygon':
                if len(i['geometry']['coordinates']) == 1:
                    shapely_input[-1]['geometry'] = unary_union(Polygon(i['geometry']['coordinates'][0]))
                else:
                    #print(i['geometry']['coordinates'])
                    polygons = []
                    for poly in i['geometry']['coordinates']:
                        polygon = Polygon(poly)
                        polygons.append(polygon)
                    shapely_input[-1]['geometry'] = unary_union(MultiPolygon(polygons))
    print("Einlesen abgeschlossen! ...")
    return shapely_input, raw_input, schema, crs

def write_shape_file(data, schema, crs, path='output_test_master.shp'):
    print("Schema für Speichern: ", schema)
    with fiona.open(path, 'w', 'ESRI Shapefile', schema, crs) as c:
        for object in data:
            c.write({
                    'geometry': mapping(object['geometry']),
                    'properties': object['properties'],
                })
    print("Ergebnis wurde in Shape-Datei gespeichert! ...")

def write_list_to_shape_file(data, data_type, crs, path):
    schema = None

    if data_type == 'Polygon':
        schema = {'geometry': 'Polygon','properties': {'ID': 'int'}}
    elif data_type == 'LineString':
        schema = {'geometry': 'LineString','properties': {'ID': 'int'}}
    elif data_type == 'Point':
        schema = {'geometry': 'Point','properties': {'ID': 'int'}}

    if schema != None:
        id = 0
        with fiona.open(path, 'w', 'ESRI Shapefile', schema, crs) as c:
            for object in data:
                c.write({
                        'geometry': mapping(object),
                        'properties': {'ID': id},
                    })
                id = id + 1
    print("Geometrie-Liste wurde in Shape-Datei gesichert ...")

            

def find_scale_factor(data, fieldname, schema, fieldname_scale_factor="Skalier"):
    field_values = []
    sum_field_values = 0
    area_values = []
    sum_area_values = 0
    anz = 0
    for object in data:
        field_values.append(float(object["properties"][fieldname]))
        area_values.append(float(object['geometry'].area))
        anz = anz + 1
        sum_field_values = sum_field_values + float(object["properties"][fieldname])
        sum_area_values = sum_area_values + float(object['geometry'].area)
    minimum_field_value = min(field_values)
    maximum_field_value = max(field_values)
    minimum_area_value = min(area_values)
    maximum_area_value = max(area_values)
    #print("Min AREA: ", minimum_area_value)
    #print("Max AREA: ", maximum_area_value)
    mean_field_value = sum_field_values / anz
    mean_area_value = sum_area_values / anz
    #print("Mean AREA: ", mean_area_value)
    #print(maximum/minimum)
    for object in data:
        #object["properties"][fieldname_scale_factor] = float(object["properties"][fieldname])/maximum_field_value
        object["properties"][fieldname_scale_factor] = ((float(object["properties"][fieldname])-minimum_field_value)/(maximum_field_value-minimum_field_value))*(maximum_area_value-minimum_area_value)+minimum_area_value
        #print("SKALIER: ", object["properties"][fieldname_scale_factor])
    schema['properties'][fieldname_scale_factor] = 'float'

    print("Skalierungsfaktoren wurden bestimmt! ...")
    return data, schema

def scale(data, fieldname_scale_factor="Skalier"):
    #print (data)
    for object in data:
        #centroid_before = object['geometry'].centroid
        factor = m.sqrt(object["properties"][fieldname_scale_factor])
        object['geometry'] = affinity.scale(object['geometry'], xfact=factor, yfact=factor, origin='centroid')
    print("Skalierung abgeschlossen! ...")
    return data

def calculate_area(data, schema, fieldname_area="Flaeche"):
    for object in data:
        object["properties"][fieldname_area] = float(object['geometry'].area)
    print("Fläche wurde berechnet! ...")

    schema['properties'][fieldname_area] = 'float'

    return data, schema

def extract_points(data):
    points = {} #{(x,y):[(country_ID1, [position1, ...]), (country_ID2, [position1, ...]), ...]}
    for object in data:
        point_position = 0
        point_list_without_last = list(object['geometry'].boundary.coords)
        point_list_without_last.pop()
        for point in point_list_without_last: #object['geometry'].boundary.coords:
            #print(points[point][0])
            if point not in points:
                points[point] = [(object['id'], [point_position])]
                point_position += 1
                """elif object['id'] not in points[point]:
                points[point].append((object['id'], [point_position]))
                point_position += 1"""
            else: # Wenn der Punkt bereits extrahiert wurde muss nun geprüft werden, ob die entsprechende Polygon-ID ebenfalls registriert wurde
                for index, element in enumerate(points[point]):
                    #print("element", element)
                    id_already_in_points = False
                    if object['id'] == element[0]:
                        #print(points[point][index][1])
                        points[point][index][1].append(point_position)
                        id_already_in_points = True
                        break
                if not id_already_in_points:
                    #print("ID not in points")
                    #print(points[point])
                    points[point].append((object['id'], [point_position]))
                    #print(points[point])
                point_position += 1
                #points[point][-1][1].append(point_position)
                #point_position += 1
    print("Punkte wurden extrahiert! ...")
    #print(points)
    return(points)

def extract_centroid_with_scale_factor(data, fieldname_scale_factor="Skalier"):
    #centroids = [(Point, Skalier, Polygon)]
    centroids = []
    for object in data:
        centroid = object['geometry'].centroid
        centroids.append((centroid.coords[0][0], centroid.coords[0][1], object['properties'][fieldname_scale_factor], object['geometry']))
    print("Zentroide wurden bestimmt! ...")
    return(centroids)

def generate_quadtree(data, points, max_depth, max_items):
    print("Quadtree wird erstellt und befüllt!...")
    #print(data)
    polygons = []
    for date in data:
        polygons.append(date['geometry'])
    vereinigungs_polygon = unary_union(polygons)
    bb = vereinigungs_polygon.envelope
    coords = mapping(bb)['coordinates']
    #print("minx: ", coords[0][0][0])
    #print("miny: ", coords[0][0][1])
    #print("maxx: ", coords[0][2][0])
    #print("maxy: ", coords[0][2][1])
    quadtree = Index((coords[0][0][0]-1, coords[0][0][1]-1, coords[0][2][0]+1, coords[0][2][1]+1), max_items = max_items, max_depth = max_depth) # "+1" bzw "-1" Damit Punkte trotz Precision-Errors auf jeden Fall innerhalb der Indexstruktur liegen

    #print("insert")
    """for date in data:
        coords = mapping(date["geometry"].envelope)['coordinates']
        quadtree.insert(date['id'], (coords[0][0][0], coords[0][0][1], coords[0][2][0], coords[0][2][1]))"""

    for index, point in enumerate(points):
        #print(point)
        #print(index)
        quadtree.insert(point, (point[0], point[1], point[0], point[1]))
    
    #print("intersect")
    #for date in data:
        #coords = mapping(date["geometry"].envelope)['coordinates']
        #print(quadtree.intersect((coords[0][0][0], coords[0][0][1], coords[0][2][0], coords[0][2][1])))
    quads = []
    i=1
    for index, obj in enumerate(quadtree):
        #print(index)
        #print(obj)
        #if obj.nodes:
            #for node in obj.nodes:
                #print(node.rect)
        #print(obj.nodes)
        
        #print(obj.children)
        if not obj.children:
            #print(i)
            i += 1
            #print(obj.center)
            #print(obj.width)
            #print(obj.height)
            x_min = obj.center[0] - obj.width/2
            x_max = obj.center[0] + obj.width/2
            y_min = obj.center[1] - obj.height/2
            y_max = obj.center[1] + obj.height/2
            quads.append(Polygon([(x_min, y_min), (x_min, y_max), (x_max, y_max), (x_max, y_min)]))
    #print("Quads: ", quads)
    #print("Anzahl: ", len(quads))

    sorted_quads = sorted(quads, key=lambda poly: poly.area, reverse=True)

    return(quadtree, sorted_quads)

def add_intersection_points_with_quads(quadtree, data, points, quads):
    print("Zusätzliche Punkte zur Topologieerhaltung werden hinzugefügt! ...")
    #print(quadtree)
    #print(data)
    #points_copy = points.copy()
    intersection_points = []
    for date in data:
        #print(date)
        poly = date['geometry']
        boundary = poly.boundary
        for quad in quads:
            quad_boundary = quad.boundary
            if boundary.intersects(quad_boundary):
                intersection_possible = True
                try:
                    intersection = boundary.intersection(quad_boundary)
                except Exception as e:
                    intersection_possible = False
                    print("Intersection nicht möglich! Exception: ", e)
                if intersection_possible and intersection.geom_type == "MultiPoint": #"LineString": #"MultiPoint":
                    for point in intersection.geoms:
                        #print(point.coords[0])
                        coords = point.coords[0]
                        coords_rounded = (round(coords[0]), round(coords[1]))
                        if coords_rounded not in intersection_points:
                            intersection_points.append(coords_rounded)
                            #points_copy
    #print(points_copy)
    #print(intersection_points)
    return intersection_points

def densify_polygon_points(data, points, quads, intersection_points):
    data_copy = data.copy()
    points_copy = points.copy()
    new_positions = {}
    for point in intersection_points:
        p = Point(point)
        has_intersection = False
        for date in data_copy:
            poly = date['geometry']
            if p.intersects(poly):
                if point[0] >= -130040 and point[0] <= -118156 and point[1] >= 22362 and point[1] <= 33110:
                    print("")
                    print(date)
                    print("Punkt intersected mit Poly-Nr: ", date['id'])
                poly_id = date['id']
                nearest_segment = None
                min_distance = float('inf')
                for i in range(len(poly.exterior.coords) - 1):
                    segment_start = poly.exterior.coords[i]
                    segment_end = poly.exterior.coords[i + 1]

                    # Berechne den Abstand des Punktes zur Kante
                    seg = LineString([segment_start, segment_end])
                    distance = p.distance(seg)

                    # Aktualisiere das nächstgelegene Segment, falls der Abstand kleiner ist
                    if distance < min_distance:
                        min_distance = distance
                        nearest_segment = (segment_start, segment_end)
                if point[0] >= -130040 and point[0] <= -118156 and point[1] >= 22362 and point[1] <= 33110:
                    print("Nearest Segment: ", nearest_segment)
                    print("Intersection Point: ", p.coords[0])
                    print(points[nearest_segment[0]])
                    print(points[nearest_segment[1]])
                #(-103600.0, 43402.0) ---
                
                
                # Herausfinden ob ein, und wenn, welches Polygon benachbart ist
                neighbor_poly_id = None
                for id_tupel1 in points[nearest_segment[0]]:
                    for id_tupel2 in points[nearest_segment[1]]:
                        if str(id_tupel1[0])!= str(date['id']) and id_tupel1[0] == id_tupel2[0]:
                            if point[0] >= -130040 and point[0] <= -118156 and point[1] >= 22362 and point[1] <= 33110:
                                print("Benachbartes Polygon: ", id_tupel1[0])
                            neighbor_poly_id = id_tupel1[0]
                            #break

                position1_in_poly1 = None
                position1_in_poly2 = None
                position2_in_poly1 = None
                position2_in_poly2 = None

                for id_tuple1 in points[nearest_segment[0]]:
                    if id_tuple1[0] == poly_id:
                        position1_in_poly1 = id_tuple1[1]
                    elif id_tuple1[0] == neighbor_poly_id:
                        position1_in_poly2 = id_tuple1[1]
                
                for id_tuple2 in points[nearest_segment[1]]:
                    if id_tuple2[0] == poly_id:
                        position2_in_poly1 = id_tuple2[1]
                    elif id_tuple2[0] == neighbor_poly_id:
                        position2_in_poly2 = id_tuple2[1]
                if point[0] >= -130040 and point[0] <= -118156 and point[1] >= 22362 and point[1] <= 33110:
                    print("Pos1 Poly1: ", position1_in_poly1)
                    print("Pos2 Poly1: ", position2_in_poly1)
                    print("Pos1 Poly2: ", position1_in_poly2)
                    print("Pos2 Poly2: ", position2_in_poly2)

                new_position_in_poly1 = None
                new_position_in_poly2 = None

                if position1_in_poly1 != None:
                    min_position1_poly1 = min(position1_in_poly1)
                    max_position1_poly1 = max(position1_in_poly1)
                    min_position2_poly1 = min(position2_in_poly1)
                    max_position2_poly1 = max(position2_in_poly1)

                    if min_position1_poly1 == 0 or min_position2_poly1 == 0:
                        #0 enthalten
                        if min_position1_poly1 == 0 and min_position2_poly1 == 1:
                            #Position 1
                            new_position_in_poly1 = 1
                        elif min_position1_poly1 == 0 and min_position2_poly1 != 1:
                            #Nach Max-Position 2
                            new_position_in_poly1 = max_position2_poly1 + 1
                        elif min_position2_poly1 == 0 and min_position1_poly1 == 1:
                            #Position 1"
                            new_position_in_poly1 = 1
                        elif min_position2_poly1 == 0 and min_position1_poly1 != 1:
                            #Nach Max-Position 1
                            new_position_in_poly1 = max_position1_poly1 + 1
                        else:
                            print("Irgendetwas ist bei der Positionsbestimmung schiefgelaufen1! Poly1")
                    else:
                        #Keine 0 enthalten
                        if min_position1_poly1 > max_position2_poly1:
                            new_position_in_poly1 = max_position2_poly1 + 1
                        elif min_position2_poly1 > max_position1_poly1:
                            new_position_in_poly1 = max_position1_poly1 + 1
                        else:
                            print("Irgendetwas ist bei der Positionsbestimmung schiefgelaufen2 Poly1!")
                        #Maximale Position aus beiden
                        #new_position_in_poly1 = max(max_position1_poly1, max_position2_poly1)

                if position1_in_poly2 != None:
                    min_position1_poly2 = min(position1_in_poly2)
                    max_position1_poly2 = max(position1_in_poly2)
                    min_position2_poly2 = min(position2_in_poly2)
                    max_position2_poly2 = max(position2_in_poly2)

                    if min_position1_poly2 == 0 or min_position2_poly2 == 0:
                        #0 enthalten
                        if min_position1_poly2 == 0 and min_position2_poly2 == 1:
                            #Position 1
                            new_position_in_poly2 = 1
                        elif min_position1_poly2 == 0 and min_position2_poly2 != 1:
                            #Nach Max-Position 2
                            new_position_in_poly2 = max_position2_poly2 + 1
                        elif min_position2_poly2 == 0 and min_position1_poly2 == 1:
                            #Position 1
                            new_position_in_poly2 = 1
                        elif min_position2_poly2 == 0 and min_position1_poly2 != 1:
                            #Nach Max-Position 1
                            new_position_in_poly2 = max_position1_poly2 + 1
                        else:
                            print("Irgendetwas ist bei der Positionsbestimmung schiefgelaufen1! Poly2")
                    else:
                        #Keine 0 enthalten
                        if min_position1_poly2 > max_position2_poly2:
                            new_position_in_poly2 = max_position2_poly2 + 1
                        elif min_position2_poly2 > max_position1_poly2:
                            new_position_in_poly2 = max_position1_poly2 + 1
                        else:
                            print("Irgendetwas ist bei der Positionsbestimmung schiefgelaufen2! Poly2")
                        #Maximale Position aus beiden
                        #new_position_in_poly2 = max(max_position1_poly2, max_position2_poly2)
                
                """if  len(position1_in_poly1) == 1 and len(position2_in_poly1) == 1:
                    if position1_in_poly1[0] < position2_in_poly1[0]:
                        new_position_in_poly1 = position1_in_poly1[0] + 1
                    else:
                        new_position_in_poly1 = position1_in_poly1[0] #- 1
                elif len(position1_in_poly1) > 1 and len(position2_in_poly1) == 1:
                    max_position = max(position1_in_poly1)
                    min_position = min(position1_in_poly1)
                    if position2_in_poly1[0] == 1:
                        new_position_in_poly1 = 1
                    else:
                        new_position_in_poly1 = max_position + 1
                elif len(position2_in_poly1) > 1 and len(position1_in_poly1) == 1:
                    if position1_in_poly1[0] == 1:
                        new_position_in_poly1 = 1
                    else:
                        new_position_in_poly1 = max(position2_in_poly1)

                if position1_in_poly2 != None and len(position1_in_poly2) == 1 and len(position2_in_poly2) == 1:
                    if position1_in_poly2[0] < position2_in_poly2[0]:
                        new_position_in_poly2 = position1_in_poly2[0] + 1
                    else:
                        new_position_in_poly2 = position1_in_poly2[0] #- 1
                elif position1_in_poly2 != None and len(position1_in_poly2) > 1:
                    if position2_in_poly2[0] == 1:
                        new_position_in_poly2 = 1
                    else:
                        new_position_in_poly2 = max(position1_in_poly2) + 1
                elif position1_in_poly2 != None and len(position2_in_poly2) > 1:
                    if position1_in_poly2[0] == 1:
                        new_position_in_poly2 = 1
                    else:
                        new_position_in_poly2 = max(position2_in_poly2)"""
                if point[0] >= -130040 and point[0] <= -118156 and point[1] >= 22362 and point[1] <= 33110:
                    print("New Pos in Poly1: ", new_position_in_poly1)
                    print("New Pos in Poly2: ", new_position_in_poly2)
                    print("Poly1 ID: ", poly_id, type(poly_id))
                    print("Poly2 ID: ", neighbor_poly_id, type(neighbor_poly_id))
                #POSITIONEN KÖNNEN MEHRFACH SEIN, FALLS MEHRERE PUNKTE AUF EINEM LINIENSEGMENT LIEGEN
                if new_position_in_poly1 != None and str(poly_id) not in list(new_positions.keys()):
                    new_positions[str(poly_id)] = [(new_position_in_poly1, p.coords[0])]
                elif new_position_in_poly1 != None:
                    new_positions[str(poly_id)].append((new_position_in_poly1, p.coords[0]))
                if new_position_in_poly2 != None and str(neighbor_poly_id) not in list(new_positions.keys()):
                    new_positions[str(neighbor_poly_id)] = [(new_position_in_poly2, p.coords[0])]
                elif new_position_in_poly2 != None:
                    new_positions[str(neighbor_poly_id)].append((new_position_in_poly2, p.coords[0]))
                has_intersection == True
                break
        if not has_intersection:
            pass
            #print("PUNKT INTERSECTED NICHT")
    
    for poly in new_positions:
        new_positions[poly].sort(key=lambda tup: tup[0])
    #print("")
    #print("NEW POSITIONS:")
    #print(new_positions)
    #print("")

    for date in data_copy:
        coordinate_list = list(date['geometry'].exterior.coords)
        #print("Is valid before: ", Polygon(coordinate_list).is_valid)
        poly_id = str(date['id'])
        sum_inserted_points = 0
        #print(coordinate_list)
        #print(new_positions[poly_id])
        #print("")
        #print("POLY ID: ", poly_id)
        #print(new_positions[poly_id])
        if poly_id in list(new_positions.keys()):
            same_position = [new_positions[poly_id][0][1]]
            """if poly_id == "1":
                print("ID 1 new_positions[poly_id]: ", new_positions[poly_id])
            if poly_id == "3":
                print("ID 3 new_positions[poly_id]: ", new_positions[poly_id])"""
            for i in range(len(new_positions[poly_id]) - 1):
                if new_positions[poly_id][i][0] == new_positions[poly_id][i+1][0]:
                    same_position.append(new_positions[poly_id][i+1][1])
                    continue
                else:
                    insert_position = new_positions[poly_id][i][0] + sum_inserted_points
                    #print(insert_position)
                    #print(same_position)
                    #print(coordinate_list)
                    if len(same_position) == 1:
                        coordinate_list.insert(insert_position, same_position[0])
                        sum_inserted_points += 1
                    else:
                        point_before = coordinate_list[insert_position - 1]
                        anz_points_inserted = 0
                        while same_position:
                            min_distance = float('inf')
                            for point in same_position:
                                diff = m.sqrt((point_before[0] - point[0])**2)
                                if diff < min_distance:
                                    nearest = point
                                    min_distance = diff
                            coordinate_list.insert(insert_position + anz_points_inserted, nearest)
                            point_before = nearest
                            same_position.remove(nearest)
                            anz_points_inserted += 1
                        sum_inserted_points += anz_points_inserted
                    same_position = [new_positions[poly_id][i+1][1]]
            # Nach der For-Schleife werden ein letztes mal Punkte eingefügt
            insert_position = new_positions[poly_id][-1][0] + sum_inserted_points
            if len(same_position) == 1:
                coordinate_list.insert(insert_position, same_position[0])
                sum_inserted_points += 1
            else:
                point_before = coordinate_list[insert_position - 1]
                anz_points_inserted = 0
                while same_position:
                    min_distance = float('inf')
                    for point in same_position:
                        diff = m.sqrt((point_before[0] - point[0])**2)
                        if diff < min_distance:
                            nearest = point
                            min_distance = diff
                    coordinate_list.insert(insert_position + anz_points_inserted, nearest)
                    point_before = nearest
                    same_position.remove(nearest)
                    anz_points_inserted += 1
                sum_inserted_points += anz_points_inserted

            
            #print(coordinate_list)
            """if poly_id == "3":
                print("ID 3 coordinate_list: ",coordinate_list)
            if poly_id == "1":
                print("ID 1 coordinate_list: ",coordinate_list)"""
            #print("POLY ID: ", poly_id)
            new_polygon = Polygon(coordinate_list)
            date['geometry'] = new_polygon
            #print(new_polygon)
            #print("")
            #print("Is valid: ", new_polygon.is_valid)
            #print("")

        #for date in data:
            #anz_stuetzpunkte = len(list(date['geometry'].exterior.coords))
            #print(f"VORHER: Polygon ID={date['id']} hat {anz_stuetzpunkte} Stützpunkte")


    for date in data_copy:
        anz_stuetzpunkte = len(list(date['geometry'].exterior.coords))
        #print(f"NACHHER: Polygon ID={date['id']} hat {anz_stuetzpunkte} Stützpunkte")
    
    return_points = extract_points(data_copy)

    
    #print("")
    #print("RETURN:")
    #print("")
    #print(return_points)
    return return_points

def densify_polygon_points1(data, points, quads, intersection_points):
    data_copy = data.copy()
    points_copy = points.copy()
    triangles_for_diff = {}
    triangles_for_union = {}
    for point in intersection_points:
        p = Point(point)
        for date in data_copy:
            poly = date['geometry']
            if p.intersects(poly):
                print("")
                print(date)
                print("Punkt intersected mit Poly-Nr: ", date['id'])
                nearest_segment = None
                min_distance = float('inf')
                for i in range(len(poly.exterior.coords) - 1):
                    segment_start = poly.exterior.coords[i]
                    segment_end = poly.exterior.coords[i + 1]

                    # Berechne den Abstand des Punktes zur Kante
                    seg = LineString([segment_start, segment_end])
                    distance = p.distance(seg)

                    # Aktualisiere das nächstgelegene Segment, falls der Abstand kleiner ist
                    if distance < min_distance:
                        min_distance = distance
                        nearest_segment = (segment_start, segment_end)
                print("Nearest Segment: ", nearest_segment)
                print("Intersection Point: ", p.coords[0])
                print(points[nearest_segment[0]])
                print(points[nearest_segment[1]])

                triangle = Polygon([nearest_segment[0], nearest_segment[1], p.coords[0]])
                # Dreieck von Polygon abziehen
                if str(date['id']) not in triangles_for_diff:
                    triangles_for_diff[str(date['id'])] = [triangle]
                else:
                    triangles_for_diff[str(date['id'])].append(triangle)
                # Dreieck mit benachbartem Polygon vereinigen
                for id_tupel1 in points[nearest_segment[0]]:
                    for id_tupel2 in points[nearest_segment[1]]:
                        if id_tupel1[0] != str(date['id']) and id_tupel1[0] == id_tupel2[0]:
                            if point == (-103600.0, 43402.0):
                                print("Benachbartes Polygon: ", id_tupel1[0])
                            if id_tupel1[0] not in triangles_for_union:
                                triangles_for_union[id_tupel1[0]] = [triangle]
                            else:
                                triangles_for_union[id_tupel1[0]].append(triangle)
                            break
    print("TRIANGLES for Diff: ", list(triangles_for_diff.keys()))
    print("TRIANGLES for Union: ", list(triangles_for_union.keys()))
    print("DIFF")

    for date in data_copy:
        anz_stuetzpunkte = len(list(date['geometry'].exterior.coords))
        print(f"VORHER: Polygon ID={date['id']} hat {anz_stuetzpunkte} Stützpunkte")

    for id in triangles_for_diff:
        #print(id)
        for date in data_copy:
            if id == date['id']:
                poly = date['geometry']
                diff = unary_union(triangles_for_diff[id])
                for triangle in triangles_for_diff[id]:
                    poly = poly.difference(triangle)
                #difference = poly.difference(diff)
                #union = unary_union([difference, poly])
                #print("Vorher: ", list(poly.exterior.coords))
                #print("Nachher: ", list(union.exterior.coords))
                #print(union)
                date['geometry'] =  poly #union
                break
    print("UNION")
    for id in triangles_for_union:
        for date in data_copy:
            if id == date['id']:
                poly = date['geometry']
                #triangles_for_diff[id].append(poly)
                #union = unary_union(triangles_for_diff[id])
                for triangle in triangles_for_diff[id]:
                    poly = poly.union(triangle)
                #print(union)
                date['geometry'] = poly
                break

    for date in data_copy:
        anz_stuetzpunkte = len(list(date['geometry'].exterior.coords))
        print(f"NACHHER: Polygon ID={date['id']} hat {anz_stuetzpunkte} Stützpunkte")
    
    return_points = extract_points(data_copy)

    
    print("")
    #print("RETURN:")
    print("")
    #print(return_points)
    return return_points


def apply_forces(points, centroids):

    define_force_field(centroids, points)

    print("Punkte wurden verschoben!...")
    points_copy = points.copy()
    new_points = {}
    for point in points_copy:
        x = point[0]
        y = point[1]
        new_point = ()
        #print("")
        #print("Point", point, points_copy[point])
        for centroid in centroids:
            #print("Centroid", centroid)
            intersection_line = LineString([(x, y), (centroid[0], centroid[1])])
            intersection = intersection_line.intersection(centroid[3].boundary)
            if intersection.geom_type == "Point":
                #print(intersection.coords[0][0], intersection.coords[0][1])
                distance = m.sqrt((x-intersection.coords[0][0])**2+(y-intersection.coords[0][1])**2)
                #print("Distanz: ", distance)
            elif intersection.geom_type == "MultiPoint":
                min_distance = None
                mean_distance_list = []
                #print("MULTIPOINT START")
                for intersection_point in intersection.geoms:
                    distance = m.sqrt((x-intersection_point.coords[0][0])**2+(y-intersection_point.coords[0][1])**2)
                    #print("Distanz: ", distance)
                    if min_distance == None or distance < min_distance:
                        min_distance = distance
                    mean_distance_list.append(distance)
                distance = sum(mean_distance_list)/len(mean_distance_list)
                #distance = min_distance
            else:
                print("kein schnittpunkt")
                #print("Min-Distanz: ", distance)
                #print("MULTIPOINT ENDE")
            #print(intersection)
            dx = x-centroid[0]
            dy = y-centroid[1]
            dx_dy = dx/dy
            dz = m.sqrt(dx**2+dy**2)
            fx = 1
            fy = 1
            c=1
            wirkdistanz = dz - distance # Die vom jeweiligen Zentroid ausgehende Kraft wirkt in dieser Distanz einfach
            """print("dz: ", dz)
            print("distance: ", distance)
            print("Wirkdistanz: ", wirkdistanz)
            print("Wirkdistanz/dz: ", wirkdistanz/dz)"""

            """print("dx: ", dx)
            print("dy: ", dy)
            print("dx/dy: ", dx_dy)
            print("dz: ", dz)"""

            #print(dx/dz)
            #print(wirkdistanz/dz)
            #print(m.sqrt(centroid[2]))
            if not new_point:
                new_point = (x+c*fx, 
                            y+c*fy)
                """new_point = (x+dx/dz*wirkdistanz/dz*m.sqrt(centroid[2]), 
                            y+dy/dz*wirkdistanz/dz*m.sqrt(centroid[2]))"""
            else:
                new_point = (new_point[0]+dx*1.4*wirkdistanz/dz*m.sqrt(centroid[2]), #dx durch Distanz mit schnittpunkt
                             new_point[1]+dy*1.4*wirkdistanz/dz*m.sqrt(centroid[2]))
            #print("Point: ", point)
            #print("New_Point: ", new_point)
        #print("NEW: ",new_point, ":", points_copy[point])
        new_points[new_point] = points_copy[point]
    #print(new_points)
    return new_points

def define_force_field(centroids, points, quads, quadtree, intersection_points, data):
    #print("TEST")
    #bla = cm.sqrt(-1)
    #print(bla**2)
    force_field = None
    #global force_field_func 
    force_field_func = None
    #print(quads)
    #Auskommentiert Force-Field Definition durch Zentroide
    """for centroid_combination in combinations(centroids, 2):
        #print(centroid_combination)
        x_pi = centroid_combination[0][0]
        x_ui = centroid_combination[1][0]
        y_pi = centroid_combination[0][1]
        y_ui = centroid_combination[1][1]
        #m_pi = centroid_combination[0][2]
        #m_ui = centroid_combination[1][2]
        #d_m_pi_m_ui = m.sqrt((m_pi - m_ui)**2)
        #print(d_m_pi_m_ui)
        d_pi_ui = m.sqrt((x_pi-x_ui)**2+(y_pi-y_ui)**2)
        x_q, y_q = symbols('x_q y_q')
        d_pi_q = sqrt((x_q-x_pi)**2+(y_q-y_pi)**2)
        soll_betrag = d_pi_ui * m.e**(-d_pi_q/d_pi_ui) #d_pi_ui * e^^(-d_pi_q/d_pi_ui)
        ist_betrag = m.sqrt((x_ui-x_pi)**2+(y_ui-y_pi)**2)

        force = ((x_ui-x_pi)*(soll_betrag/ist_betrag),
                (y_ui-y_pi)*(soll_betrag/ist_betrag))
        
        if force_field == None:
            force_field = force
        else:
            force_field = (force_field[0] + force[0],
                           force_field[1] + force[1])"""
            
    points_copy = points.copy()

    # Force-Feld Definition durch sämtliche Vertices
    """print("Force-Field wird bestimmt!...")
    for centroid in centroids:
        for x_pi, y_pi in centroid[3].exterior.coords:
            #print(x_pi, y_pi)
            if centroid[2]!=1:
                x_ui = (x_pi - centroid[0]) * m.sqrt(centroid[2]) + centroid[0]
                y_ui = (y_pi - centroid[1]) * m.sqrt(centroid[2]) + centroid[1]

                d_pi_ui = m.sqrt((x_pi-x_ui)**2+(y_pi-y_ui)**2)
                x_q, y_q = symbols('x_q y_q')
                d_pi_q = sqrt((x_q-x_pi)**2+(y_q-y_pi)**2)
                soll_betrag = d_pi_ui * m.e**(-d_pi_q/d_pi_ui) #d_pi_ui * e^^(-d_pi_q/d_pi_ui)
                ist_betrag = m.sqrt((x_ui-x_pi)**2+(y_ui-y_pi)**2)

                force = ((x_ui-x_pi)*(soll_betrag/ist_betrag),
                         (y_ui-y_pi)*(soll_betrag/ist_betrag))
            
                print("DEBUG INFORMATION")
                print(force)
                print(x_pi, y_pi)
                print(x_ui, y_ui)
                print(centroid[2])
                print(d_pi_q)
                print(soll_betrag)
                print(ist_betrag)
        
            if force_field == None:
                force_field = force
            else:
                force_field = (force_field[0] + force[0], force_field[1] + force[1])"""
    
    # Auskommentiert: Force-Field Definition durch Polygon-approximierende Kreise
    print("Force-Field wird bestimmt!...")
    for centroid in centroids:
        if centroid[2] != -1: # IF-STATEMENT KANN WEG
            ist_area = centroid[3].area
            ist_radius = m.sqrt(ist_area/m.pi) #Durchschnittlicher Radius des Polygon-approximierenden Kreises
            soll_radius = m.sqrt(centroid[2]/m.pi)
            #print("ist_radius: ", ist_radius)
            #print("soll_radius: ", soll_radius)
            anz_points = 4
            phi = 2*m.pi/anz_points
            for i in range(anz_points):
                x_pi = centroid[0] + ist_radius * m.cos(i*phi)
                y_pi = centroid[1] + ist_radius * m.sin(i*phi)
                x_ui = centroid[0] + soll_radius * m.cos(i*phi)
                y_ui = centroid[1] + soll_radius * m.sin(i*phi)
                """
                if i == 0:
                    x_pi = centroid[0] + ist_radius
                    y_pi = centroid[1]
                    x_ui = centroid[0] + soll_radius
                    y_ui = centroid[1]
                elif i == 1:
                    x_pi = centroid[0] - ist_radius
                    y_pi = centroid[1]
                    x_ui = centroid[0] - soll_radius
                    y_ui = centroid[1]
                elif i == 2:
                    x_pi = centroid[0]
                    y_pi = centroid[1] + ist_radius
                    x_ui = centroid[0]
                    y_ui = centroid[1] + soll_radius
                else:
                    x_pi = centroid[0]
                    y_pi = centroid[1] - ist_radius
                    x_ui = centroid[0]
                    y_ui = centroid[1] - soll_radius"""

                #x_ui = (x_pi - centroid[0]) * m.sqrt(centroid[2]) + centroid[0]
                #y_ui = (y_pi - centroid[1]) * m.sqrt(centroid[2]) + centroid[1]
                #force_func = force_function(x_pi,y_pi,x_ui,y_ui)
                force_func = force_function_alternative(x_pi, y_pi, centroid[0], centroid[1], ist_area, centroid[2])
                #force_field_func = add_summand(force_field_func, force_func)
                if force_field_func == None:
                    force_field_func = force_func
                else:
                    force_field_func = add_summand(force_field_func, force_func)
                #SYMPY
                """d_pi_ui = m.sqrt((x_pi-x_ui)**2+(y_pi-y_ui)**2)
                if d_pi_ui != 0:
                    x_q, y_q = symbols('x_q y_q')
                    d_pi_q = sqrt((x_q-x_pi)**2+(y_q-y_pi)**2)
                    soll_betrag = d_pi_ui * m.e**(-d_pi_q/d_pi_ui) #d_pi_ui * e^^(-d_pi_q/d_pi_ui)
                    ist_betrag = m.sqrt((x_ui-x_pi)**2+(y_ui-y_pi)**2)

                    force = ((x_ui-x_pi)*(soll_betrag/ist_betrag),
                            (y_ui-y_pi)*(soll_betrag/ist_betrag))
                    
                    if False:
                        print("")
                        print("force: ", force)
                        print("ist_radius: ", ist_radius)
                        print("soll_radius: ", soll_radius)
                        print("x_pi: ", x_pi)
                        print("y_pi: ", y_pi)
                        print("x_ui: ", x_ui)
                        print("y_ui: ", y_ui)
                        print("d_pi_ui: ", d_pi_ui)
                        print("d_pi_q: ", d_pi_q)
                        print("soll_betrag: ", soll_betrag)
                        print("ist_betrag: ", ist_betrag)
                        print("force: ", force)
                        
                
                    if force_field == None:
                        force_field = force
                    else:
                        force_field = (force_field[0] + force[0], force_field[1] + force[1])"""

    #SYMPY
    #force_x_dx = diff(force_field[0], x_q)
    #force_y_dx = diff(force_field[1], x_q)
    #force_x_dy = diff(force_field[0], y_q)
    #force_y_dy = diff(force_field[1], y_q)


    print("Elastizitätskoeffizienten werden bestimmt!...")

    """Elasitzitätskoeefizienten nur aus Quads"""
    import multiprocessing
    list_of_local_c_values = []
    epsilon = sys.float_info.epsilon
    
    quad_point_list = []
    for quad in quads:
        quad_points = mapping(quad)['coordinates'][0]
        for point in quad_points:
            if point not in quad_point_list:
                quad_point_list.append(point)
    #print("QuadPointList")
    #print(quad_point_list)
    print("Anzahl Quad-Points: ", len(quad_point_list))
    print("Anzahl Intersection-Points: ", len(intersection_points))
    debug_anzahl = 0
    multi_core_start_time = time.time()
    try:
        num_cores = multiprocessing.cpu_count()
        pool = multiprocessing.Pool(processes=num_cores)
        all_points_list = quad_point_list + intersection_points
        #print("All_points_List: ", all_points_list)
        #print("Args: ", arguments)
        arguments = [(p, force_field_func) for p in all_points_list]
        #print("force_field_function", type(force_field_func))
        list_of_local_c_values = pool.starmap(calc_local_elasticity_coefficients, arguments)
    except KeyboardInterrupt:
        pool.terminate()
        pool.join()
    #print("Result Multi-Core:")
    #print(list_of_local_c_values)
    pool.close()
    pool.join()
    multi_core_end_time = time.time()
    """print("Multi ist abgeschlossen!")
    single_core_start_time = time.time()
    for point in all_points_list:
        x_diff = diff_x(force_field_func, point[0], point[1])
        y_diff = diff_y(force_field_func, point[0], point[1])
        eval_force_x_dx = x_diff[0] #force_x_dx.evalf(subs=values)
        eval_force_y_dx = x_diff[1] #force_y_dx.evalf(subs=values)
        eval_force_x_dy = y_diff[0] #force_x_dy.evalf(subs=values)
        eval_force_y_dy = y_diff[1] #force_y_dy.evalf(subs=values)
        
        a = eval_force_x_dx * eval_force_y_dy - eval_force_x_dy * eval_force_y_dx
        b = eval_force_x_dx + eval_force_y_dy
        c_local = None

        if a==0 and b<0:
            c_local = (epsilon - 1)/b
        elif a!=0 and b**2>4*a: #*(1-epsilon):
            c_local = (-b-m.sqrt((b**2)-(4*a*(1-epsilon))))/(2*a)
        else:
            c_local = 1
        if c_local < 0 or c_local > 1:
            c_local = 1
        
        list_of_local_c_values.append(c_local)"""
    
    """print("Ab hier wieder alten Code!")
    for quad in quads:
        #print("Quad: ", quad)
        quad_points = mapping(quad)['coordinates'][0]
        #print("QuadPoints")
        #print(quad_points)
        for index, point in enumerate(quad_points):
            #debug_anzahl += 1
            #SYMPY
            #values = {x_q: point[0], y_q: point[1]}
            #if index == 1:
                #start = time.time()
            x_diff = diff_x(force_field_func, point[0], point[1])
            y_diff = diff_y(force_field_func, point[0], point[1])
            eval_force_x_dx = x_diff[0] #force_x_dx.evalf(subs=values)
            eval_force_y_dx = x_diff[1] #force_y_dx.evalf(subs=values)
            eval_force_x_dy = y_diff[0] #force_x_dy.evalf(subs=values)
            eval_force_y_dy = y_diff[1] #force_y_dy.evalf(subs=values)
            #if index == 1:
                #ende = time.time()
                #print("Diff-Zeit: ", ende-start)
            #print("DEBUG")
            #print(force_field_func(point[0], point[1]))
            #print(diff_x(force_field_func, point[0], point[1]))
            #print(eval_force_x_dx, eval_force_y_dx)
            #if index == 1:
                #start = time.time()
            a = eval_force_x_dx * eval_force_y_dy - eval_force_x_dy * eval_force_y_dx
            b = eval_force_x_dx + eval_force_y_dy
            c_local = None

            if a==0 and b<0:
                c_local = (epsilon - 1)/b
            elif a!=0 and b**2>4*a: #*(1-epsilon):
                c_local = (-b-m.sqrt((b**2)-(4*a*(1-epsilon))))/(2*a)
            else:
                c_local = 1
            #print("c_local: ", c_local)
            if c_local < 0 or c_local > 1:
                #print("c_local auf 1 gesetzt")
                c_local = 1
        
            list_of_local_c_values.append(c_local)
            #if index == 1:
                #ende = time.time()
                #print("C-Zeit: ", ende-start)

    #print("Debug Anzahl Points mit Doppelten Quadpunkten: ", debug_anzahl)
    #print("Elastizitätskoeffizienten aus zusätzlichen Punkten werden bestimmt!...")
    for intersection_point in intersection_points:
        x_diff = diff_x(force_field_func, intersection_point[0], intersection_point[1])
        y_diff = diff_y(force_field_func, intersection_point[0], intersection_point[1])
        eval_force_x_dx = x_diff[0] #force_x_dx.evalf(subs=values)
        eval_force_y_dx = x_diff[1] #force_y_dx.evalf(subs=values)
        eval_force_x_dy = y_diff[0] #force_x_dy.evalf(subs=values)
        eval_force_y_dy = y_diff[1] #force_y_dy.evalf(subs=values)
        a = eval_force_x_dx * eval_force_y_dy - eval_force_x_dy * eval_force_y_dx
        b = eval_force_x_dx + eval_force_y_dy
        c_local = None

        if a==0 and b<0:
            c_local = (epsilon - 1)/b
        elif a!=0 and b**2>4*a: #*(1-epsilon):
            c_local = (-b-m.sqrt((b**2)-(4*a*(1-epsilon))))/(2*a)
        else:
            c_local = 1
        #print("c_local: ", c_local)
        if c_local < 0 or c_local > 1:
            #print("c_local auf 1 gesetzt")
            c_local = 1
        
        list_of_local_c_values.append(c_local)"""

    #print("Result Single-Core:")
    #print(list_of_local_c_values)
    #single_core_end_time = time.time()

    #single_core_time = single_core_end_time - single_core_start_time
    multi_core_time = multi_core_end_time - multi_core_start_time
    #print("Single-Core Zeit: ", single_core_time)
    print("Elastizitätskoeffizienten-Zeit: ", multi_core_time)
    c_global = min(list_of_local_c_values)

    print("Neue Quadkoordinaten werden berechnet !...")
    new_quad_coordinates_start = time.time()
    try:
        num_cores = multiprocessing.cpu_count()
        pool = multiprocessing.Pool(processes=num_cores)
        arguments = [(q, force_field_func, c_global) for q in quads]
        new_quads = pool.starmap(calc_new_coordinates, arguments) #calc_new_coordinates(quad, force_field_func, c_global)
    except KeyboardInterrupt:
        pool.terminate()
        pool.join()
    #print("Result Multi-Core:")
    #print(new_quads)
    pool.close()
    pool.join()
    new_quad_coordinates_end = time.time()
    new_quad_coordinates_time = new_quad_coordinates_end - new_quad_coordinates_start
    print("Quad-Koordinatenberechnung Zeit: ", new_quad_coordinates_time)
    """new_quads = []
    for quad in quads:
        quad_points = mapping(quad)['coordinates'][0]
        point_list = []
        for point in quad_points:
            #values = {x_q: point[0], y_q: point[1]}
            #print("FORCE_FIELD[0]: ", force_field[0].evalf(subs=values))
            #print("FORCE_FIELD[1]: ", force_field[1].evalf(subs=values))
            #print("Point: ", point)
            point_list.append((point[0] + c_global * force_field_func(point[0], point[1])[0],
                               point[1] + c_global * force_field_func(point[0], point[1])[1]))
            #SYMPY
            #point_list.append((point[0] + c_global * force_field[0].evalf(subs=values),
                    #point[1] + c_global * force_field[1].evalf(subs=values)))
        
        new_quad = Polygon(point_list)
        new_quads.append(new_quad)"""


    """Elastizitätskoeffizienten aus Polygon Vertices"""
    """list_of_local_c_values = []
    
    for point in points_copy:
        #print(point)
        values = {x_q: point[0], y_q: point[1]}
        eval_force_x_dx = force_x_dx.evalf(subs=values)
        eval_force_y_dx = force_y_dx.evalf(subs=values)
        eval_force_x_dy = force_x_dy.evalf(subs=values)
        eval_force_y_dy = force_y_dy.evalf(subs=values)
        a = eval_force_x_dx * eval_force_y_dy - eval_force_x_dy * eval_force_y_dx
        b = eval_force_x_dx + eval_force_y_dy
        c_local = None
        epsilon = sys.float_info.epsilon

        if a==0 and b<0:
            c_local = (epsilon - 1)/b
        elif a!=0 and b**2>4*a: #*(1-epsilon):
            c_local = (-b-m.sqrt((b**2)-(4*a*(1-epsilon))))/(2*a)
        else:
            c_local = 1
        #print("c_local: ", c_local)
        if c_local < 0 or c_local > 1:
            #print("c_local auf 1 gesetzt")
            c_local = 1
        
        list_of_local_c_values.append(c_local)

    c_global = min(list_of_local_c_values)
    print("C (global): ", c_global)"""

    

    print("Neue Punktkoordinaten werden berechnet!...")
    #densified_points = points_copy #densify_polygon_points(data, points, quads, intersection_points)
    densified_points = densify_polygon_points(data, points, quads, intersection_points)
    #print("ANzahl Points: ", len(points))
    #print("ANzahl DENSIFIED Points: ", len(densified_points))
    new_points = {}
    """for point in points_copy:
        values = {x_q: point[0], y_q: point[1]}
        new_point = (point[0] + c_global * force_field[0].evalf(subs=values),
                    point[1] + c_global * force_field[1].evalf(subs=values))
        #print(new_point)
        new_points[new_point] = points_copy[point]"""
    for point in densified_points:
        #print(point)
        #quadtree.intersects((point[0], point[1], point[0], point[1]))
        shapely_point = Point(point)
        intersection_bool = False
        for index, quad in enumerate(quads): # An dieser Stelle ist es wichtig, dass quads der Fläche nach sortiert ist (absteigend)
            if shapely_point.intersects(quad):
                intersection_bool = True
                quad_coords = quad.exterior.coords[:]
                x_quad_coords = [p[0] for p in quad_coords]
                y_quad_coords = [p[1] for p in quad_coords]

                new_quad_coords = new_quads[index].exterior.coords[:]
                x_new_quad_coords = [p[0] for p in new_quad_coords]
                y_new_quad_coords = [p[1] for p in new_quad_coords]

                f_x = interp2d(x_quad_coords, y_quad_coords, x_new_quad_coords, kind='linear')
                f_y = interp2d(x_quad_coords, y_quad_coords, y_new_quad_coords, kind='linear')

                new_x = f_x(point[0], point[1]) 
                new_y = f_y(point[0], point[1])
                new_point = (new_x[0], new_y[0])
                #print(type(point))
                #print(point)
                #print(type(point[0]))
                #print(type(point[1]))
                #print(type(points_copy))
                #print(type(new_point))
                #print(new_point)
                #print(type(new_point[0]))
                #print(new_point[0])
                #print(type(new_point[1]))
                #print(new_point[1])
                #print(type(new_points))

                new_points[new_point] = densified_points[point]
                break
        if not intersection_bool:
            print("Point Intersected nicht")

    return new_points, new_quads
        #print(point)


def rebuild_polygons_from_points(points, old_polygons):
    polygons = old_polygons.copy()
    #print(polygons)
    poly_points = {}
    for point in points:
        for polygon_ids in points[point]:
            #print(polygon_ids, point)
            id = polygon_ids[0]
            positions = polygon_ids[1]
            if id not in poly_points:
                poly_points[id] = []
            for position in positions:
                while len(poly_points[id]) <= position:
                    poly_points[id].append(None)
                poly_points[id][position] = point
            """else:
                for position in positions:
                    while len(poly_points[id]) <= position:
                        poly_points[id].append(None)
                    poly_points[id][position] = point"""
    #print("")
    #print(poly_points)
    for polygon in polygons:
        #print(polygon)
        try:
            polygon['geometry']=Polygon(poly_points[polygon['id']])
        except Exception as e:
            print(poly_points[polygon['id']])
            print("Exception: ", e)
            print("ERROR: Nicht alle Polygone konnten wieder zusammengesetzt werden")
    #print("")
    #print(polygons)
    print("Polygone wurden generiert! ...")
    return(polygons)
    # TEST
    """
    test = []
    testindex = 3
    print(test)
    print(len(test))
    while len(test)<=testindex:
        test.append(None)
    test[testindex] = 3
    print(test)
    test[2] = 2
    print(test)"""
    

if __name__ == "__main__":
    fieldname = "jahr_2013" #"Wert" 
    max_depth = 4
    max_items = 1
    iterations = 10
    variable_depth = True

    input_data, raw_input, schema, crs = input_polygon_shape_file(".\data\\alter15-u21.shp")#.\data\\alter15-u21.shp") #".\data\master_test.shp" #.\data\\alter15-u21.shp
    #num_cores = multiprocessing.cpu_count()
    sys.setrecursionlimit(10000)
    #print("Anzahl der Kerne:", num_cores)
    deepening_per_iteration = max_depth/iterations
    for i in range(iterations):
        if variable_depth:
            if iterations == i+1:
                depth = max_depth
            else:
                depth = round(deepening_per_iteration*(i+1), ndigits=None)
        else:
            depth = max_depth
        if depth < 1:
            depth = 1
        print("Durchlauf: ", i+1)
        print("Quadtree-Tiefe: ", depth)
        if i == 0:
            data_with_scale_factor, schema = find_scale_factor(input_data, fieldname, schema)
        else:
            data_with_scale_factor, schema = find_scale_factor(new_polygons, fieldname, schema)
        #print(data_with_scale_factor)
        #print(data_with_scale_factor)

        points = extract_points(data_with_scale_factor)
        #print(points)
        centroids = extract_centroid_with_scale_factor(data_with_scale_factor)
        #NOPE new_points = apply_forces(points, centroids)
        quadtree, quads = generate_quadtree(data_with_scale_factor, points, depth, max_items)
        intersection_points = add_intersection_points_with_quads(quadtree, data_with_scale_factor, points, quads)
        #densify_polygon_points(data_with_scale_factor, points, quads, intersection_points)
        write_list_to_shape_file(quads, 'Polygon', crs, "quads_last.shp")
        #polygons_plus_intersection_with_quads = add_intersection_points_with_quads(quadtree, data_with_scale_factor, points, quads)
        #points_plus_intersection_with_quads = extract_points(polygons_plus_intersection_with_quads)
        #new_points, new_quads = define_force_field(centroids, points_plus_intersection_with_quads, quads)

        new_points, new_quads = define_force_field(centroids, points, quads, quadtree, intersection_points, data_with_scale_factor)
        write_list_to_shape_file(new_quads, 'Polygon', crs, "new_quads_last.shp")
        new_polygons = rebuild_polygons_from_points(new_points, data_with_scale_factor)


        #output_data = scale(data_with_scale_factor)
        #output_data_with_area, schema = calculate_area(new_polygons, schema, "FlaecheNachher")
        #print(output_data)


    write_shape_file(new_polygons, schema, crs, "output_alter15-u18_shrink_enlarge3_10iter_variable_maxdepth4.shp")